#!/usr/bin/env scheme-script
;; Loko Scheme kernel
;; Copyright © 2019-2020 Göran Weinholt
;; SPDX-License-Identifier: AGPL-3.0+
#!r6rs

;;; REPL that runs on a framebuffer.

;; Sorry for the mess.

(import
  (rnrs)
  (rnrs eval)
  (only (loko) parameterize make-parameter pretty-print include)
  (loko match)
  (loko system fibers)
  (loko system unsafe)

  (only (loko system $primitives) $void?)
  (only (loko runtime repl) banner)
  (only (loko system r7rs) current-output-port* current-error-port*)

  (only (psyntax expander) interaction-environment new-interaction-environment)

  (loko drivers pci)
  (loko drivers video bga)
  (loko font font-6x13)

  (loko drivers mouse)
  (loko drivers keyboard)
  (loko drivers ps2 core)
  (loko drivers ps2 i8042)
  (loko drivers ps2 mouse)
  (loko drivers ps2 keyboard)
  (loko drivers usb hid-numbers)

  (loko drivers ata core)
  (loko drivers ata drive)
  (loko drivers ata ide)
  (loko drivers ata identify)
  (loko drivers pci)
  (loko drivers storage)

  (fs partitions common)
  (fs partitions mbr)
  (fs partitions gpt)
  (fs fatfs)
  (only (loko system $host) install-vfs dma-allocate)

  (loko net internet)
  (loko net dhcpv4-client)
  (loko net tcp)
  (loko drivers net)
  ;; (loko drivers net eepro100)
  ;; (loko drivers net rtl8139)
  (loko drivers net virtio)

  (text-mode console)
  (text-mode console events)
  (text-mode console model))

(define devs (pci-scan-bus #f))


(define font-h 13)
(define font-w 6)

;;; Bochs Graphics Array

;; TODO: Make a nice API for getting any kind of fb working, with a mouse

(define w 1280)
(define h 720)

(define framebuffer
  (let ((dev (find probe·pci·bga? devs)))
    (when (not dev)
      (error #f "Could not find Bochs graphics"))
    (let ((bga (make·pci·bga dev)))
      (bga-set-mode bga w h 32 #t #t)
      (bga-framebuffer-address bga))))

(define framebuffer-copy
  (let ((&buf (dma-allocate (* w h 4) -1)))
    (do ((i 0 (fx+ i 4)))
        ((fx=? i (* w h 4)))
      (put-mem-u32 (fx+ &buf i) 0))
    &buf))

(define (set-pixel x y c)
  (let ((offset (fx* (fx+ x (fx* y w)) 4)))
    (when (fx<=? 0 offset (fx- (fx* (fx* w h) 4) 4))
      (put-mem-u32 (fx+ framebuffer-copy offset) c))))

;; Mouse pointer by Paer Martinsson
(include "pointer.scm")

(define mouse-x (div w 2))
(define mouse-y (div h 2))
(define mouse-w 32)
(define mouse-h 32)
(define mouse-hx 0)
(define mouse-hy 0)

(define bounding-box                    ;x0,y0--x1,y2
  (vector #f #f #f #f))

(define (extend-bounding-box! x y)
  (let ((x (fxmin (fxmax 0 x) (fx- w 1)))
        (y (fxmin (fxmax 0 y) (fx- h 1))))
    (match bounding-box
      [#(x0 y0 x1 y1)
       (cond ((not x0)
              (vector-set! bounding-box 0 x)
              (vector-set! bounding-box 1 y)
              (vector-set! bounding-box 2 (fx+ x 1))
              (vector-set! bounding-box 3 (fx+ y 1)))
             (else
              (vector-set! bounding-box 0 (fxmin x0 x))
              (vector-set! bounding-box 1 (fxmin y0 y))
              (vector-set! bounding-box 2 (fxmax x1 (fx+ x 1)))
              (vector-set! bounding-box 3 (fxmax y1 (fx+ y 1)))))])))

(define (framebuffer-redraw)
  ;; Redraw all pixels within the bounding box
  (match bounding-box
    [#(x0 y0 x1 y1)
     (when x0
       (do ((y y0 (fx+ y 1))
            (fy (fx* y0 (fx* w 4)) (fx+ fy (fx* w 4))))
           ((fx=? y y1))
         (do ((x x0 (fx+ x 1))
              (idx (fx+ fy (fx* x0 4)) (fx+ idx 4)))
             ((fx=? x x1))
           (put-mem-u32 (fx+ framebuffer idx) (get-mem-u32 (fx+ framebuffer-copy idx)))))
       (vector-set! bounding-box 0 #f))])
  ;; Draw the mouse pointer. Proper graphics hardware usually has some
  ;; special way to do this.
  (do ((y 0 (fx+ y 1)))
      ((fx=? y mouse-h))
    (do ((x 0 (fx+ x 1)))
        ((fx=? x mouse-w))
      (let ((x (fx+ mouse-x x)) (y (fx+ mouse-y y))
            (c (bytevector-u32-ref pointer (fx* 4 (fx+ x (fx* y mouse-w))) (endianness little))))
        (unless (eqv? 0 (fxarithmetic-shift-right c 24)) ;alpha
          (when (and (fx<? -1 x w) (fx<? -1 y h))
            (let ((offset (fx* (fx+ x (fx* y w)) 4)))
              (put-mem-u32 (fx+ framebuffer offset) c))))))))

(define (move-mouse-cursor x y)
  (extend-bounding-box! mouse-x mouse-y)
  (extend-bounding-box! (fx+ mouse-x mouse-w) (fx+ mouse-y mouse-h))
  (set! mouse-x x)
  (set! mouse-y y)
  (extend-bounding-box! mouse-x mouse-y)
  (extend-bounding-box! (fx+ mouse-x mouse-w) (fx+ mouse-y mouse-h))
  (framebuffer-redraw))

;;; text-mode backend for the framebuffer

(define mouse-manager (make-mouse-manager))
(define keyboard-manager (make-keyboard-manager))

(define (manage-ps/2 controller)
  (define hotplug-channel (make-channel))
  (let lp ()
    (match (get-message (PS/2-controller-notify-channel controller))
      [('new-port . port)
       ;; At this point we should probe the port, find a driver for
       ;; the device and spawn a fiber to handle it.
       (let ((reset-id
              (guard (exn ((error? exn) #f))
                (PS/2-command port PS/2-RESET)
                (let ((result (PS/2-read port 4000)))
                  (PS/2-read port 1000)))))
         (cond
           ((probe·PS/2·mouse port)
            => (lambda (id)
                 #;(println (list 'mouse id port))
                 (spawn-fiber (lambda ()
                                (let ((mouse (make-managed-mouse mouse-manager)))
                                  (driver·PS/2·mouse port hotplug-channel id mouse))))))
           ((probe·PS/2·keyboard port)
            => (lambda (id)
                 #;(println (list 'keyboard id port))
                 (spawn-fiber (lambda ()
                                (let ((keyboard (make-managed-keyboard keyboard-manager)))
                                  (driver·PS/2·keyboard port hotplug-channel id keyboard))))))
           (else
            ;; Probing failed, we have no driver. Start the hotplug
            ;; driver, that should tell us when a new device has been
            ;; attached.
            (spawn-fiber (lambda ()
                           (driver·PS/2·hotplug port hotplug-channel))))))])
    (lp)))

;; Convert a text-mode color to a framebuffer color
(define (tm-color->fb-color c) c)

(define (draw-character font x y fg bg ch)
  (extend-bounding-box! x y)
  (extend-bounding-box! (fx+ x font-w) (fx+ y font-h))
  (let lp ((ch ch))
    (let* ((i (char->integer ch))
           (page-num (fxarithmetic-shift-right i 8)))
      (cond ((fx<? page-num (vector-length font))
             (let* ((page (vector-ref font page-num))
                    (bitmap (and (vector? page)
                                 (vector-ref page (fxand i #xff)))))
               (if (bytevector? bitmap)
                   (do ((i 0 (fx+ i 1)))
                       ((fx=? i font-h))
                     (do ((y (fx+ y i))
                          (bitmask (bytevector-u8-ref bitmap i))
                          (xm #x80 (fxarithmetic-shift-right xm 1))
                          (xe (fx+ x font-w))
                          (x x (fx+ x 1)))
                         ((fx=? x xe))
                       (let ((c (if (eqv? 0 (fxand xm bitmask))
                                    (if (fx=? bg Default)
                                        (fxdiv y 4)
                                        bg)
                                    (if (fx=? fg Default)
                                        (rgb-color #xaa #xaa #xaa)
                                        fg))))
                         (set-pixel x y c))))
                   (lp #\nul))))
            (else
             (lp #\nul))))))

(define text-cursor-color Gray)

(define text-mode-event-ch (make-channel))

(define (framebuffer-textmode-backend)
  (define cols (fxdiv w font-w))
  (define rows (fxdiv h font-h))
  (lambda (cmd arg)
    (case cmd
      [(get-size)
       (values cols rows w h)]
      [(init)
       #f]
      [(update redraw)
       (let* ((c arg)
              ;; Cursor location in absolute coordinates
              (cx (fx+ (console-x c) (console-x1 c)))
              (cy (fx+ (console-y c) (console-y1 c))))
         (assert (fx=? cols (console-full-cols c)))
         (assert (fx=? rows (console-full-rows c)))
         (do ((y 0 (fx+ y 1))) ((fx=? y rows))
           (let ((ch-y (fx* font-h y)))
             (when (console-row-dirty? c y 'absolute)
               (let-values ([(buf mbuf fgbuf bgbuf abuf idx) (%index/nowin c 0 y)])
                 (do ((x 0 (fx+ x 1))) ((fx=? x cols))
                   (if (and (eqv? x cx) (eqv? y cy))
                       (draw-character font (fx* font-w cx) ch-y
                                       (rgb-color 0 0 0) text-cursor-color
                                       #\space)
                       ;; TODO: Bold, italics, etc
                       (let ((ch (text-ref buf (fx+ idx x))))
                         (unless (textcell-unused? ch)
                           (let ((fg (tm-color->fb-color (fg-ref fgbuf (fx+ idx x))))
                                 (bg (tm-color->fb-color (bg-ref bgbuf (fx+ idx x))))
                                 (ch-x (fx* font-w x)))
                             (draw-character font ch-x ch-y fg bg ch))))))))))
         (clear-console-dirty! c)
         (set-console-dirty! c (console-y c))
         (framebuffer-redraw))]
      [(read-event)
       (get-message text-mode-event-ch)]
      [else
       #f])))

(define (text-mode-keyboard&mouse-driver)
  (define (hid->key-symbol page usage)
    ;; FIXME: recover the key location
    (case page
      ((#x07)
       (cond ((assv usage
                    '((#x28 . Enter)
                      (#x2a . Backspace)
                      (#x2b . Tab)
                      (#x4a . Home)
                      (#x4b . PageUp)
                      (#x4c . Delete)
                      (#x4d . End)
                      (#x4e . PageDown)
                      (#x4f . ArrowRight)
                      (#x50 . ArrowLeft)
                      (#x51 . ArrowDown)
                      (#x52 . ArrowUp)))
              => cdr)
             (else #f)))
      (else #F)))
  (define (hid->modifier-set leds mods)
    (define (fxtest? mods mask)
      (not (eqv? 0 (fxand mods mask))))
    ((enum-set-constructor (modifier-set))
     (filter values
             (list
              (and (fxtest? mods HID-modifiers-Control) (modifier ctrl))
              (and (fxtest? mods HID-modifiers-Shift) (modifier shift))
              (and (fxtest? mods HID-modifiers-Alt) (modifier alt))
              (and (fxtest? mods HID-modifiers-GUI) (modifier meta))
              #;(and (fxtest? mods HID-modifiers-AltGr) (modifier alt-graph))
              #;(and (fxbit-set? leds HID-LED-Caps-Lock) (modifier caps-lock))
              #;(and (fxbit-set? leds HID-LED-Num-Lock) (modifier num-lock))
              #;(and (fxbit-set? leds HID-LED-Scroll-Lock) (modifier scroll-lock))))))
  (define mouse-scale 1)
  (let lp ()
    ;; XXX: It is tempting to separate the keyboard and the mouse, but
    ;; the user can hold modifiers on the keyboard while using the
    ;; mouse
    (match (perform-operation
            (choice-operation
             (wrap-operation (get-operation
                              (mouse-event-channel mouse-manager))
                             (lambda (x) (cons 'mouse x)))
             (wrap-operation (get-operation
                              (keyboard-event-channel keyboard-manager))
                             (lambda (x) (cons 'kbd x)))))
      [('mouse . #(xd yd zd buttons _))
       ;; XXX: y axis is inverted, as it should always be
       (let ((x (fxmax 0 (fxmin (* mouse-scale (fx- w 1)) (fx+ mouse-x xd))))
             (y (fxmax 0 (fxmin (* mouse-scale (fx- h 1)) (fx- mouse-y yd)))))
         ;;(println (list x y 'mouse xd yd buttons))
         (move-mouse-cursor x y)
         (lp))]
      [('kbd . #(make/break _ page usage (leds mods key)))
       (let ((modifiers (hid->modifier-set leds mods)))
         (when (eq? make/break 'make)
           (cond
             ((hid->key-symbol page usage) =>
              (lambda (keysym)
                (put-message text-mode-event-ch
                             (make-key-press-event modifiers key keysym 'standard))))
             ((char? key)
              (put-message text-mode-event-ch
                           (make-key-press-event modifiers key key 'standard)))
             (else
              (put-message text-mode-event-ch
                           (make-unknown-event 'keyboard (list page usage key)))))))
       (lp)]
      [('kbd . event)
       (put-message text-mode-event-ch (make-unknown-event 'keyboard event))
       (lp)])))

(current-console (make-console (framebuffer-textmode-backend)))

;;; the repl

(define (println/newlines str)
  (let ((p (open-string-input-port str)))
    (let lp ()
      (let ((line (get-line p)))
        (unless (eof-object? line)
          (println line)
          (lp))))))

(define (print/write datum)
  (print
   (call-with-string-output-port
     (lambda (p) (write datum p)))))

;; TODO: Support styles on the standard implementation of print-condition
(define (print-condition exn)
  (cond ((condition? exn)
         (let ((c* (simple-conditions exn)))
           (text-color LightRed)
           (println "An unhandled condition was raised:")
           (do ((i 1 (fx+ i 1))
                (c* c* (cdr c*)))
               ((null? c*))
             (let* ((c (car c*))
                    (rtd (record-rtd c)))
               (text-color Default)
               (print " ")
               (let loop ((rtd rtd))
                 (text-attribute 'bold (eq? rtd (record-rtd c)))
                 (print (record-type-name rtd))
                 (text-attribute 'bold #f)
                 (cond ((record-type-parent rtd) =>
                        (lambda (rtd)
                          (unless (eq? rtd (record-type-descriptor &condition))
                            (print " ")
                            (loop rtd))))))
               (let loop ((rtd rtd))
                 (do ((f* (record-type-field-names rtd))
                      (i 0 (fx+ i 1)))
                     ((fx=? i (vector-length f*))
                      (cond ((record-type-parent rtd) => loop)))
                   (println)
                   (print "  ")
                   (text-color Default)
                   (text-attribute 'italic #t)
                   (print (vector-ref f* i))
                   (print ": ")
                   (text-attribute 'italic #f)
                   (let ((x ((record-accessor rtd i) c)))
                     (cond ((and (eq? rtd (record-type-descriptor &irritants))
                                 (pair? x) (list? x))
                            (print "(")
                            (let ((list-x (wherex)))
                              (text-color LightRed)
                              (print/write (car x))
                              (for-each (lambda (value)
                                          (println)
                                          (gotoxy list-x (wherey))
                                          (print/write value))
                                        (cdr x)))
                            (text-color Default)
                            (print ")"))
                           (else
                            (text-color LightRed)
                            (print/write x)))))))
             (println))))
        (else
         (println "A non-condition object was raised:")
         (print/write exn))))

(define (make-console-output-port color)
  (define id "console")
  (define (write! str start count)
    (text-color color)
    (do ((count count (fx- count 1))
         (i 0 (fx+ i 1)))
        ((eqv? count 0))
      (let ((c (string-ref str i)))
        (cond ((eqv? c #\newline)
               (println)
               #;(update-screen)
               )
              (else
               (print (string c))))))
    count)
  (define get-position #f)
  (define set-position! #f)
  (define close #f)
  (make-custom-textual-output-port id write! get-position set-position! close))

(define (eval-expr datum env)
  (call/cc
    (lambda (k)
      (with-exception-handler
        (lambda (exn)
          ;; TODO: what about warnings?
          ;; TODO: flush output ports?
          (let ((p (current-error-port)))
            (text-color Default)
            (print-condition exn))
          (k #t))
        (lambda ()
          (current-output-port* (make-console-output-port Default))
          (current-error-port* (make-console-output-port LightRed))
          (call-with-values
            ;; TODO: should redirect input to console and use parameters
            (lambda ()
              (let-values ((v* (eval datum (env))))
                (flush-output-port (current-output-port))
                (flush-output-port (current-error-port))
                (apply values v*)))
            (case-lambda
              ((x)
               (cond (($void? x)
                      (text-color DarkGray)
                      (write x))
                     (else
                      (pretty-print x))))
              (()
               (text-color DarkGray)
               (println "No return values"))
              (x*
               (for-each pretty-print x*))))
          (flush-output-port (current-output-port))
          (flush-output-port (current-error-port)))))))

(define env (make-parameter #f))

(define (tui)
  (define line "")
  (define cur 0)
  (define prev #f)

  ;; TODO: This process should receive messages for windows, wait for
  ;; input and dispatch it to either another process or the window
  ;; handling, and output screen updates.

  (unless (interaction-environment)
    (interaction-environment (new-interaction-environment)))
  (parameterize ((env (interaction-environment)))
    (do ((stop #f)) (stop)

      (gotoxy 0 (wherey))
      (text-color LightGreen)
      (print "> ")
      (let ((prompt-width (wherex)))
        (text-color Default)
        (print line)
        (clreol)
        (gotoxy (fx+ cur prompt-width) (wherey)))

      (update-screen)
      (let ((ev (read-event)))
        (set! text-cursor-color Gray)
        ;; Debug printing
        (when #f
          (cond
            ((key-press-event? ev)
             (print (list 'press (enum-set->list (keyboard-event-mods ev))
                          (keyboard-event-char ev) (keyboard-event-key ev)
                          (keyboard-event-location ev))))
            ((unknown-event? ev)
             (print (list 'unknown (unknown-event-source ev)
                          (if (integer? (unknown-event-data ev))
                              (number->string (unknown-event-data ev) 16)
                              (unknown-event-data ev)))))
            (else
             (print ev)))
          (clreol))

        (cond
          ((and (key-press-event? ev)
                (enum-set=? (keyboard-event-mods ev) (modifier-set ctrl))
                (eqv? (keyboard-event-key ev) #\l))
           (clrscr)
           (gotoxy 0 0))

          ((resize-event? ev)
           #f)

          ((and (key-press-event? ev)
                (or (and
                      (enum-set=? (keyboard-event-mods ev) (modifier-set ctrl))
                      (eqv? (keyboard-event-key ev) #\a))
                    (eq? (keyboard-event-key ev) 'Home)))
           (set! cur 0))

          ((and (key-press-event? ev)
                (or (and
                      (enum-set=? (keyboard-event-mods ev) (modifier-set ctrl))
                      (eqv? (keyboard-event-key ev) #\e))
                    (eq? (keyboard-event-key ev) 'End)))
           (set! cur (string-length line)))

          ((and (key-press-event? ev) (eq? (keyboard-event-key ev) 'Backspace)
                (enum-set=? (enum-set-difference (keyboard-event-mods ev)
                                                 (modifier-set shift))
                            (modifier-set)))
           (unless (eqv? cur 0)
             (set! line (string-append (substring line 0 (fx- cur 1))
                                       (substring line cur (string-length line))))
             (set! cur (fx- cur 1))))

          ((and (key-press-event? ev) (eq? (keyboard-event-key ev) 'ArrowLeft)
                (not (keyboard-event-has-modifiers? ev)))
           (unless (eqv? cur 0)
             (set! cur (fx- cur 1))))

          ((and (key-press-event? ev) (eq? (keyboard-event-key ev) 'ArrowRight)
                (not (keyboard-event-has-modifiers? ev)))
           (unless (eqv? cur (string-length line))
             (set! cur (fx+ cur 1))))

          ((and (key-press-event? ev) (eq? (keyboard-event-key ev) 'ArrowUp)
                (not (keyboard-event-has-modifiers? ev)))
           (set! line prev)
           (set! cur (string-length line)))

          ((and (key-press-event? ev) (eq? (keyboard-event-key ev) 'Enter)
                (not (keyboard-event-has-modifiers? ev)))
           (unless (equal? line "")
             (set! prev line)
             (call/cc
               (lambda (k)
                 (guard (exn ((lexical-violation? exn)
                              (set! text-cursor-color LightRed)
                              (k #t)))
                   (let* ((line-p (open-string-input-port line))
                          (datum (read line-p)))
                     (set! line (if (port-eof? line-p) "" (get-string-all line-p)))
                     (set! cur 0)
                     (println)
                     (eval-expr datum env)))))))

          ((and (key-press-event? ev) (keyboard-event-char ev)) =>
           (lambda (char)
             (set! line (string-append (substring line 0 cur)
                                       (string char)
                                       (substring line cur (string-length line))))
             (set! cur (fx+ cur 1)))))))))

(define (print-lines string)
  (let ((p (open-string-input-port string)))
    (let lp ()
      (let ((line (get-line p)))
        (unless (eof-object? line)
          (println line)
          (lp))))))

(clrscr)
(gotoxy 0 0)
(text-color Gray)
(print-lines (call-with-string-output-port banner))
(update-screen)

(spawn-fiber
 (lambda ()
   (let ((controller (make-PS/2-controller)))
     (spawn-fiber (lambda () (manage-ps/2 controller)))
     (driver·isa·i8042 controller))))

(spawn-fiber text-mode-keyboard&mouse-driver)

(spawn-fiber tui)

;;; Screen refresh

(define (screen-refresh)
  (extend-bounding-box! 0 0)
  (extend-bounding-box! w h)
  (let lp ((i 0))
    (update-screen)
    (sleep 1/10)
    (lp (+ i 1))))


;;; Disk stuff

(define logch (make-channel))

(define (string-split str c)
  (let lp ((start 0) (end 0))
    (cond ((fx=? end (string-length str))
           (list (substring str start end)))
          ((char=? c (string-ref str end))
           (cons (substring str start end)
                 (lp (fx+ end 1) (fx+ end 1))))
          (else
           (lp start (fx+ end 1))))))

(spawn-fiber
 (lambda ()
   (let lp ()
     (let ((msg (get-message logch)))
       (println msg))
     (lp))))

(define (log/info msg)
  (put-message logch msg))

(define (log-device-info identify-block)
  (let-values ([(logical physical) (ata-identify:ata-sector-size identify-block)])
    (log/info (list 'model: (ata-identify:model-number identify-block)
                    'serial: (ata-identify:serial-number identify-block)
                    'firmware: (ata-identify:firmware-revision identify-block)
                    'commands: (ata-identify:supported-command-set identify-block)
                    'major-version: (ata-identify:major-revision identify-block)
                    (cond ((ata-identify:ata-device? identify-block) 'ata:)
                          ((ata-identify:atapi-device? identify-block) 'atapi:)
                          (else 'unknown-identify:))
                    (if (ata-identify:ata-device? identify-block)
                        (list 'logical-sector-size: logical
                              'physical-sector-size: physical)
                        (list))
                    'raw: identify-block))))

(define storage-manager-ch (make-channel))

(define (make-sector-reader storage lba-start lba-end)
  (lambda (lba)
    (cond
      ((or (not lba-end) (<= lba-start lba lba-end))
       (let ((resp-ch (make-channel)))
         (put-message (storage-device-request-channel storage)
                      (list resp-ch 'read (+ lba-start lba) 1))
         (match (get-message resp-ch)
           [('ok data)
            data]
           [('error . x)
            (log/info (list "Error reading from medium" lba x))
            (eof-object)])))
      (else
       (log/info "Out of range read")
       (eof-object)))))

(define (storage-device->fatfs-device storage-device starting-lba ending-lba)
  (define logical-sector-size
    (storage-device-logical-sector-size storage-device))
  (define (read-sectors lba sectors)
    (assert (and (fx>=? lba 0) (fx<=? 1 sectors 1024)))
    (let ((resp-ch (make-channel)))
      (let* ((read-lba (+ lba starting-lba))
             (read-end-lba (+ read-lba sectors)))
        (unless (or (not ending-lba) (<= read-end-lba ending-lba))
          (assertion-violation #f "Out of range storage device read"
                               storage-device lba))
        (put-message (storage-device-request-channel storage-device)
                     (list resp-ch 'read read-lba sectors)))
      (match (get-message resp-ch)
        [('ok data) data]
        [('error . x) (error #f "Read error" storage-device x)])))
  (define (write-sectors lba data)
    (let ((resp-ch (make-channel)))
      (let* ((sectors (fxdiv (bytevector-length data) logical-sector-size))
             (write-lba (+ lba starting-lba))
             (write-end-lba (+ write-lba sectors)))
        (unless (or (not ending-lba) (<= write-end-lba ending-lba))
          (assertion-violation #f "Out of range storage device read"
                               storage-device lba))
        (put-message (storage-device-request-channel storage-device)
                     (list resp-ch 'write write-lba data)))
      (match (get-message resp-ch)
        [('ok) (if #f #f)]
        [('error . x)
         (error #f "Write error" storage-device x)])))
  (define (flush) #f)
  (define (close) #f)
  (make-fatfs-device logical-sector-size
                     read-sectors
                     write-sectors
                     flush
                     close))

(define (fatfs-directory-files fs dirname)
  (let ((dir (fatfs-open-directory fs dirname)))
    (let lp ((ret '()))
      (let ((info (fatfs-read-directory dir)))
        (cond ((eof-object? info)
               (fatfs-close-directory dir)
               (reverse ret))
              (else
               (lp (cons (or (fatfs-file-info:long-filename info)
                             (fatfs-file-info:filename info))
                         ret))))))))

(spawn-fiber
 (lambda ()
   (let lp ()
     (match (get-message storage-manager-ch)
       [('new-storage storage)
        (log/info (list 'storage: storage))
        (spawn-fiber
         (lambda ()
           ;; Scan the partition table and list root directories of
           ;; FAT filesystems
           #;
           (let ((resp-ch (make-channel)))
             (put-message (storage-device-request-channel storage)
                          (list resp-ch 'read
                                ;; Read sector 0 on normal media and sector 16 on CD/DVD
                                (if (eqv? (storage-device-logical-sector-size storage) 2048)
                                    16
                                    0)
                                1))
             (log/info (list 'sector: (get-message resp-ch))))
           (let ((read-sector (make-sector-reader storage 0 #f))
                 (sector-size (storage-device-logical-sector-size storage)))
             (let ((mbr (read-mbr read-sector)))
               ;; XXX: Real applications should probably check the MBR
               ;; to verify that it's a protective MBR. It could have
               ;; been overwritten with a valid MBR, in which case
               ;; gpt1 is likely an out of date GPT backup.
               (let-values ([(gpt0 gpt1) (read-gpt read-sector #f)])
                 (log/info (list "Partition table:" mbr gpt0 gpt1))
                 (let ((table (cond ((parttable-valid? gpt0) gpt0)
                                    ((parttable-valid? gpt1) gpt1)
                                    (else mbr))))
                   (for-each
                    (lambda (part)
                      (define GPT-TYPE-EFI-System
                        #vu8(#xC1 #x2A #x73 #x28
                                  #xF8 #x1F #x11 #xD2 #xBA #x4B
                                  #x00 #xA0 #xC9 #x3E #xC9 #x3B))
                      (log/info (list "Partition:" part))
                      (when (or (member (part-type part)
                                        '(1 4 6 #xb #xc #xd #xe #xef))
                                (equal? (part-type part) GPT-TYPE-EFI-System))
                        (let* ((dev (storage-device->fatfs-device storage
                                                                  (part-start-lba part)
                                                                  (part-end-lba part)))
                               (fs (open-fatfs dev)))
                          (define (parse-filename who filename)
                            (if (string=? filename "/")
                                '()
                                (let ((components (string-split filename #\/)))
                                  (cond ((and (not (string=? filename ""))
                                              (char=? (string-ref filename 0) #\/))
                                         (cdr components))
                                        (else
                                         (condition
                                          (make-i/o-filename-error filename)
                                          (make-who-condition who)
                                          (make-message-condition "Bad filename")))))))
                          (define (open-file filename file-options buffer-mode who)
                            (define no-create (enum-set-member? 'no-create file-options))
                            (define no-fail (enum-set-member? 'no-fail file-options))
                            (define no-truncate (enum-set-member? 'no-truncate file-options))
                            (define create (not no-create))
                            (define fail (not no-fail))
                            (define truncate (not no-truncate))
                            ;; (log/info (list filename file-options buffer-mode who))
                            (let ((path/err (parse-filename 'open-file filename))
                                  (flags (case who
                                           ((open-file-output-port open-file-input/output-port)
                                            (if (and fail create)
                                                (fxior fatfs-open/create fatfs-open/exclusive)
                                                (if truncate
                                                    (if (and no-fail create)
                                                        (fxior fatfs-open/create fatfs-open/truncate)
                                                        fatfs-open/truncate)
                                                    (if (and no-fail create)
                                                        fatfs-open/create
                                                        0))))
                                           (else 0)))
                                  (access-mode (case who
                                                 ((open-file-input-port) fatfs-open/read)
                                                 ((open-file-output-port) fatfs-open/write)
                                                 ((open-file-input/output-port) fatfs-open/read+write))))
                              (if (or (pair? path/err) (null? path/err))
                                  (guard (exn
                                          ((and (who-condition? exn)
                                                (eq? (condition-who exn) 'fatfs-open-file))
                                           (raise
                                             (condition exn (make-i/o-filename-error filename)))))
                                    (fatfs-open-file fs path/err (fxior access-mode flags)))
                                  (raise
                                    (condition path/err
                                               (make-irritants-condition
                                                (list filename file-options buffer-mode)))))))
                          (define (file-exists? fn)
                            (let ((path/err (parse-filename 'file-exists? fn)))
                              (if (or (pair? path/err) (null? path/err))
                                  (fatfs-file-exists? fs path/err)
                                  (raise path/err))))
                          (define (directory-files fn hidden?)
                            (let ((path/err (parse-filename 'directory-files fn)))
                              (if (or (pair? path/err) (null? path/err))
                                  (fatfs-directory-files fs path/err)
                                  (raise path/err))))
                          (log/info fs)
                          (log/info (list "FAT root directory:"
                                          (fatfs-directory-files fs '())))
                          (install-vfs 'open-file open-file
                                       'file-exists? file-exists?
                                       'directory-files directory-files
                                       ))))
                    (parttable-partitions table))))))))])
     (lp))))

(define (ata-manager controller)
  (let lp ()
    (match (get-message (ata-controller-notify-channel controller))
      [('new-device . ch)
       ;; Probe the device and start a driver
       (spawn-fiber
        (lambda ()
          (match (probe·ata ch)
            [('ata . identify-block)
             (log/info "ATA drive")
             (log-device-info identify-block)
             (let-values ([(logical _) (ata-identify:ata-sector-size identify-block)])
               (let* ((atadev (make-ata-device controller ch identify-block))
                      (storage (make-storage-device "ATA drive" logical)))
                 (put-message storage-manager-ch (list 'new-storage storage))
                 (driver·ata·drive atadev storage)))]

            #;
            [('atapi . identify-block)
             (log/info "ATAPI device")
             (log-device-info identify-block)
             ;; TODO: Make a new SCSI logical unit and set the ATAPI
             ;; device as the request channel
             (let ((scsi-req-ch (make-channel)))
               (put-message scsi-manager-ch (cons 'new-device scsi-req-ch))
               ;; XXX: The ATAPI driver currently prints some debug
               ;; stuff and there's a concurrency bug around ports...
               (sleep 5)
               (driver·ata·atapi (make-ata-device controller ch identify-block)
                                 scsi-req-ch))]

            ['no-device
             (log/info "No device")]

            [x
             (log/info (list "No driver:" x))])))
       (lp)])))


(log/info "Scanning for IDE devices...")

(for-each (lambda (dev)
            (when (probe·pci·ide? dev)
              (log/info "Found an IDE controller")
              (log/info dev)
              (let ((controller (make-ata-controller)))
                (spawn-fiber (lambda () (ata-manager controller)))
                (spawn-fiber (lambda () (driver·pci·ide dev controller))))))
          devs)

(for-each (lambda (dev)
            (cond ((probe·pci·virtio-net? dev)
                   (log/info "Found a virtio network card")
                   (log/info dev)
                   (let ((iface (make-iface 'ethernet)))
                     (spawn-fiber (lambda () (driver·pci·virtio-net dev iface)))
                     (let* ((ether (make-ether iface))
                            (tcp (make-tcp ether)))
                       (spawn-fiber (lambda () (net·ethernet·internet-stack ether)))
                       (spawn-fiber (lambda () (net·ethernet·dhcpv4-client ether)))
                       (spawn-fiber (lambda () (net·tcp/ip tcp))))))))
          devs)

(screen-refresh)
