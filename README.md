# Loko Scheme

Loko Scheme is an implementation of the algorithmic language Scheme.
It runs on Linux/amd64, NetBSD/amd64 and on bare amd64 hardware.
Both the R6RS and the R7RS standards are supported.

Loko Scheme is intended to be a platform for application and operating
system development. It is written purely in Scheme and some assembler
(i.e. no C code at the bottom).

## Current status

Loko fails just a single test from Racket's R6RS test suite. A handful
of tests from Chibi's R7RS test suite fail, mostly having to do with
floating point. R7RS support is newer and less well tested.

Memory management is limited. Processes can use at most 1 GB of RAM
(half of which is usable). The limit can be increased, but the memory
manager needs some arbitrary limit.

The packages in [Akku](https://akkuscm.org) work for the most part,
but Loko is still new and packages using non-standard features are
less likely to have been ported yet.

## Documentation

[The Loko Scheme Developer's Manual](https://scheme.fail/manual/loko.html) is
available online. It is also available
in [PDF format](https://scheme.fail/manual/loko.pdf).

The manual can also be build from the Texinfo sources with `make
manual`.

## Building

See the section
[Building Loko](https://scheme.fail/manual/loko.html#Building) in
the manual.

## Contact

* [Loko Scheme on GitLab issues](https://gitlab.com/weinholt/loko/issues)
* The IRC channel `#loko` on Freenode, but `#scheme` also works if the
  subject is about Scheme in general.
* The Usenet group comp.lang.scheme, available through any Usenet
  provider,
  e.g. [Eternal September](http://www.eternal-september.org/). I would
  like for Scheme communities to be less fragmented, so I will wait
  with setting up a separate mailing list.
* The [Loko Scheme](https://scheme.fail/) website.

## License

**The samples/ and srfi/ directories are covered by a different license.**

Copyright © 2019-2020 Göran Weinholt

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
