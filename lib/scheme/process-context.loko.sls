;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: AGPL-3.0-or-later
;; Loko Scheme - an R6RS Scheme compiler
;; Copyright © 2020 Göran Weinholt

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
#!r6rs

;;; R7RS-small standard library

(library (scheme process-context)
  (export
    command-line emergency-exit (rename (r7rs-exit exit))
    get-environment-variable
    get-environment-variables)
  (import
    (rnrs)
    (srfi :98 os-environment-variables)
    (only (loko system r7rs) emergency-exit))

(define (translate-status status)
  (case status
    ((#t) 0)
    ((#f) 1)
    (else status)))

(define r7rs-exit
  (case-lambda
    (()
     (exit))
    ((status)
     (exit (translate-status status))))))
