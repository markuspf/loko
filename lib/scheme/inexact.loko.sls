;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: AGPL-3.0-or-later
;; Loko Scheme - an R6RS Scheme compiler
;; Copyright © 2020 Göran Weinholt

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
#!r6rs

;;; R7RS-small standard library

(library (scheme inexact)
  (export
    acos asin atan cos exp finite? infinite? log nan? sin sqrt tan)
  (import
    (except (rnrs) finite? infinite? nan?)
    (prefix (rnrs) r6:))

(define (finite? z)
  (if (complex? z)
      (and (r6:finite? (real-part z))
           (r6:finite? (imag-part z)))
      (r6:finite? z)))

(define (infinite? z)
  (if (complex? z)
      (or (r6:infinite? (real-part z))
          (r6:infinite? (imag-part z)))
      (r6:infinite? z)))

(define (nan? z)
  (if (complex? z)
      (or (r6:nan? (real-part z))
          (r6:nan? (imag-part z)))
      (r6:nan? z))))
