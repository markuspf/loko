;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: AGPL-3.0-or-later
;; Loko Scheme - an R6RS Scheme compiler
;; Copyright © 2020 Göran Weinholt

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
#!r6rs

;;; R7RS-small standard library

(library (scheme write)
  (export
    display write write-shared write-simple)
  (import
    (except (rnrs) display write)
    (only (loko) parameterize print-dialects)
    (prefix (only (loko) display write) loko:)
    (prefix (only (loko system r7rs) write-shared write-simple) loko:))

(define display
  (case-lambda
    ((obj)
     (display obj (current-output-port)))
    ((obj port)
     (parameterize ([print-dialects '(r7rs)])
       (loko:display obj port)))))

(define write
  (case-lambda
    ((obj)
     (write obj (current-output-port)))
    ((obj port)
     (parameterize ([print-dialects '(r7rs)])
       (loko:write obj port)))))

(define write-shared
  (case-lambda
    ((obj)
     (write-shared obj (current-output-port)))
    ((obj port)
     (parameterize ([print-dialects '(r7rs)])
       (loko:write-shared obj port)))))

(define write-simple
  (case-lambda
    ((obj)
     (write-shared obj (current-output-port)))
    ((obj port)
     (parameterize ([print-dialects '(r7rs)])
       (loko:write-simple obj port))))))
