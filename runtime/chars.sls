;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: AGPL-3.0-or-later
;; Loko Scheme - an R6RS Scheme compiler
;; Copyright © 2019, 2020 Göran Weinholt

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
#!r6rs

;;; Characters

(library (loko runtime chars)
  (export
    char? char->integer integer->char
    char=? char<? char>? char<=? char>=?)
  (import
    (except (rnrs)
            char? char->integer integer->char
            char=? char<? char>? char<=? char>=?
            char-upcase char-downcase char-titlecase char-foldcase
            char-ci=? char-ci<? char-ci>? char-ci<=? char-ci>=?
            char-alphabetic? char-numeric? char-whitespace?
            char-upper-case? char-lower-case? char-title-case?
            char-general-category)
    (prefix (rnrs) sys:))

(define (char? x) (sys:char? x))

(define (char->integer x) (sys:char->integer x))

(define (integer->char sv) (sys:integer->char sv))

(define-syntax define-char-predicate
  (lambda (x)
    (syntax-case x ()
      ((_ name same? ->obj)
       #'(define name
           (case-lambda
             ((x y)
              (same? (->obj x) (->obj y)))
             ((x y z)
              (let ((x (->obj x)) (y (->obj y)) (z (->obj z)))
                (and (same? x y) (same? y z))))
             ((x y^ . y*^)
              (let lp ((x y^) (y* y*^) (ret (same? (->obj x) (->obj y^))))
                (if (null? y*)
                    ret
                    (let ((y (car y*)))
                      (lp y (cdr y*) (and (same? (->obj x) (->obj y)) ret))))))))))))

(define-char-predicate char=? eq? (lambda (x) (char->integer x) x))

(define-char-predicate char<? fx<? char->integer)

(define-char-predicate char>? fx>? char->integer)

(define-char-predicate char<=? fx<=? char->integer)

(define-char-predicate char>=? fx>=? char->integer))
