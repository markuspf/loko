;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: AGPL-3.0-or-later
;; Loko Scheme - an R6RS Scheme compiler
;; Copyright © 2019, 2020 Göran Weinholt

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
#!r6rs

;;; A place for various utilities

;; This should be kept portable. It is used when bootstrapping.

(library (loko runtime utils)
  (export
    map-in-order
    string-split
    strip-akku-prefix
    bytevector-hash
    string-hash*)
  (import
    (rnrs))

(define map-in-order
  (case-lambda
    ((f x*)
     (let lp ((ret '()) (x* x*))
       (if (null? x*)
           (reverse ret)
           (lp (cons (f (car x*)) ret)
               (cdr x*)))))
    ((f x* y*)
     (let lp ((ret '()) (x* x*) (y* y*))
       (if (null? x*)
           (reverse ret)
           (lp (cons (f (car x*) (car y*)) ret)
               (cdr x*) (cdr y*)))))
    ((f x* y* z*)
     (let lp ((ret '()) (x* x*) (y* y*) (z* z*))
       (if (null? x*)
           (reverse ret)
           (lp (cons (f (car x*) (car y*) (car z*)) ret)
               (cdr x*) (cdr y*) (cdr z*)))))
    ((f x* y* . z**)
     (let lp ((ret '()) (x* x*) (y* y*) (z** z**))
       (if (null? x*)
           (reverse ret)
           (lp (cons (apply f (car x*) (car y*) (map car z**)) ret)
               (cdr x*) (cdr y*) (map cdr z**)))))))

(define (string-split str c)
  (let lp ((start 0) (end 0))
    (cond ((fx=? end (string-length str))
           (list (substring str start end)))
          ((char=? c (string-ref str end))
           (cons (substring str start end)
                 (lp (fx+ end 1) (fx+ end 1))))
          (else
           (lp start (fx+ end 1))))))

(define (strip-akku-prefix x)
  (define prefix ".akku/lib/")
  (define plen (string-length prefix))
  (let lp ((i 0))
    (cond ((and (fx>=? (string-length x) (fx+ plen i))
                (string=? prefix (substring x i (fx+ plen i))))
           (substring x (fx+ i plen) (string-length x)))
          ((fx<? i (fx- (string-length x) plen))
           (lp (fx+ i 1)))
          (else x))))

;;; FNV-1 hashes

;; These are used in various situations when hashing objects both
;; during bootstrap and normal run time. The hashes can be encoded in
;; 32 bits.

(define bytevector-hash
  (case-lambda
    ((bv)
     (define offset_basis 2166136261)
     (bytevector-hash bv offset_basis))
    ((bv d)
     (define FNV_prime 16777619)
     (if (> (fixnum-width) 57)
         (do ((i 0 (fx+ i 1))
              (d d (fxand (fxxor (fx* d FNV_prime)
                                 (bytevector-u8-ref bv i))
                          #xffffffff)))
             ((fx=? i (bytevector-length bv)) d))
         (do ((i 0 (fx+ i 1))
              (d d (bitwise-and (bitwise-xor (* d FNV_prime)
                                             (bytevector-u8-ref bv i))
                                #xffffffff)))
             ((fx=? i (bytevector-length bv)) d))))))

(define string-hash*
  (case-lambda
    ((s)
     (define offset_basis 2166136261)
     (string-hash* s offset_basis))
    ((s d)
     (define FNV_prime 16777619)
     (if (> (fixnum-width) 57)
         (do ((i (fx- (string-length s) 1) (fx- i 1))
              (d d (do ((b (char->integer (string-ref s i))
                           (fxarithmetic-shift-right b 8))
                        (d d (fxand (fxxor (fx* d FNV_prime)
                                           (fxand b #xff))
                                    #xffffffff)))
                       ((eqv? b 0) d))))
             ((eqv? i -1) d))
         (do ((i (fx- (string-length s) 1) (fx- i 1))
              (d d (do ((b (char->integer (string-ref s i))
                           (bitwise-arithmetic-shift-right b 8))
                        (d d (bitwise-and (bitwise-xor (* d FNV_prime)
                                                       (bitwise-and b #xff))
                                          #xffffffff)))
                       ((eqv? b 0) d))))
             ((eqv? i -1) d)))))))
