;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: AGPL-3.0-or-later
;; Loko Scheme - an R6RS Scheme compiler
;; Copyright © 2019, 2020 Göran Weinholt

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
#!r6rs

;;; Bytevectors

(library (loko runtime bytevectors)
  (export
    native-endianness bytevector? make-bytevector bytevector-length
    bytevector=? bytevector-fill! bytevector-copy! bytevector-copy

    bytevector-u8-ref bytevector-s8-ref
    bytevector-u8-set! bytevector-s8-set!
    bytevector->u8-list u8-list->bytevector
    bytevector-uint-ref bytevector-sint-ref
    bytevector-uint-set! bytevector-sint-set!
    bytevector->uint-list bytevector->sint-list
    uint-list->bytevector sint-list->bytevector
    bytevector-u16-ref bytevector-s16-ref
    bytevector-u16-native-ref bytevector-s16-native-ref
    bytevector-u16-set! bytevector-s16-set!
    bytevector-u16-native-set! bytevector-s16-native-set!
    bytevector-u32-ref bytevector-s32-ref
    bytevector-u32-native-ref bytevector-s32-native-ref
    bytevector-u32-set! bytevector-s32-set!
    bytevector-u32-native-set! bytevector-s32-native-set!
    bytevector-u64-ref bytevector-s64-ref
    bytevector-u64-native-ref bytevector-s64-native-ref
    bytevector-u64-set! bytevector-s64-set!
    bytevector-u64-native-set! bytevector-s64-native-set!
    bytevector-ieee-single-native-ref bytevector-ieee-single-ref
    bytevector-ieee-double-native-ref bytevector-ieee-double-ref
    bytevector-ieee-single-native-set! bytevector-ieee-single-set!
    bytevector-ieee-double-native-set! bytevector-ieee-double-set!
    string->utf8 string->utf16 string->utf32
    utf8->string utf16->string utf32->string

    bytevector-address)
  (import
    (except (rnrs)
            native-endianness bytevector? make-bytevector bytevector-length
            bytevector=? bytevector-fill! bytevector-copy! bytevector-copy
            bytevector-u8-ref bytevector-s8-ref
            bytevector-u8-set! bytevector-s8-set!
            bytevector->u8-list u8-list->bytevector
            bytevector-uint-ref bytevector-sint-ref
            bytevector-uint-set! bytevector-sint-set!
            bytevector->uint-list bytevector->sint-list
            uint-list->bytevector sint-list->bytevector
            bytevector-u16-ref bytevector-s16-ref
            bytevector-u16-native-ref bytevector-s16-native-ref
            bytevector-u16-set! bytevector-s16-set!
            bytevector-u16-native-set! bytevector-s16-native-set!
            bytevector-u32-ref bytevector-s32-ref
            bytevector-u32-native-ref bytevector-s32-native-ref
            bytevector-u32-set! bytevector-s32-set!
            bytevector-u32-native-set! bytevector-s32-native-set!
            bytevector-u64-ref bytevector-s64-ref
            bytevector-u64-native-ref bytevector-s64-native-ref
            bytevector-u64-set! bytevector-s64-set!
            bytevector-u64-native-set! bytevector-s64-native-set!
            bytevector-ieee-single-native-ref bytevector-ieee-single-ref
            bytevector-ieee-double-native-ref bytevector-ieee-double-ref
            bytevector-ieee-single-native-set! bytevector-ieee-single-set!
            bytevector-ieee-double-native-set! bytevector-ieee-double-set!
            string->utf8 string->utf16 string->utf32
            utf8->string utf16->string utf32->string)
    (prefix (rnrs) sys:)
    (prefix (only (loko system unsafe) bytevector-address) sys:)
    (loko system $primitives))

;; Answers the question: are the
;; bytevector-{s,u}{8,16,32}-{native-,}{ref,set!} procedures
;; open-coded by the code generator on this architecture when the
;; endianness argument is known?
(define-syntax opencoded?
  (lambda (x)
    (syntax-case x (ref set! u8)
      ;; All archs must have bytevector-u8-{ref,set!} open-coded.
      ((_ ref u8) #'#t)
      ((_ set! u8) #'#t)
      ;; TODO: handle non-opencoding of the native procedures.
      ((_ ref size)
       #'(memq 'size '(s8 u16 s16 u32 s32)))
      ((_ set! size)
       #'#f))))

(define (native-endianness)
  (endianness little))

(define (bytevector? x) (sys:bytevector? x))

(define make-bytevector
  (case-lambda
    ;; XXX: assumes uninitialized memory is zeroed
    ((len) (make-bytevector len 0))
    ((len fill)
     (assert (fx>=? len 0))
     (assert (fx<=? -128 fill 255))
     (if (eqv? len 0)
         #vu8()
         (let ((bv ($make-bytevector len)))
           (unless (eqv? fill 0)
             (bytevector-fill! bv fill))
           bv)))))

(define (bytevector-length x) (sys:bytevector-length x))

(define (bytevector=? bv1 bv2)
  (let ((len (bytevector-length bv1)))
    (and (fx=? len (bytevector-length bv2))
         (or
           (eq? bv1 bv2)
           (let ((len32 (fxand len -4)))
             (let lp ((i 0))
               (cond ((fx=? i len32)
                      (do ((i i (fx+ i 1))
                           (diff 0 (fxior diff
                                          (fxxor
                                           (bytevector-u8-ref bv1 i)
                                           (bytevector-u8-ref bv2 i)))))
                          ((fx=? i len) (fxzero? diff))))
                     ((not (fx=? (bytevector-u32-native-ref bv1 i)
                                 (bytevector-u32-native-ref bv2 i)))
                      #f)
                     (else
                      (lp (fx+ i 4))))))))))

(define (bytevector-fill! bv fill)
  (assert (fx<=? -128 fill 255))
  (let* ((fill8 (fxbit-field fill 0 8))
         (fill32 (fx* #x01010101 fill8))
         (end8 (bytevector-length bv))
         (end32 (fxand end8 -4)))
    (do ((i 0 (fx+ i 4)))
        ((fx=? i end32)
         (do ((i end32 (fx+ i 1)))
             ((fx=? i end8))
           (bytevector-u8-set! bv i fill8)))
      (bytevector-u32-native-set! bv i fill32))))

;; This should definitely be open-coded in a lot of cases. It should
;; make use of how objects are aligned in memory, etc.
(define (bytevector-copy! s ss t ts k)
  (assert (fx>=? k 0))
  (assert (and (bytevector? s) (bytevector? t)))
  (let copy! ((ss ss) (ts ts) (k k))
    (cond ((and (eq? t s) (fx<? ss ts))
           (do ((i (fx- k 1) (fx- i 1)))
               ((fx=? i -1))
             (bytevector-u8-set! t (fx+ ts i)
                                 (bytevector-u8-ref s (fx+ ss i)))))
          ((eqv? 0 (fxand #b11 (fxior ss ts)))
           (do ((k^ (fxand k -4))
                (i 0 (fx+ i 4)))
               ((fx=? i k^)
                (do ((i i (fx+ i 1)))
                    ((fx=? i k))
                  (bytevector-u8-set! t (fx+ ts i)
                                      (bytevector-u8-ref s (fx+ ss i)))))
             (bytevector-u32-native-set! t (fx+ ts i)
                                         (bytevector-u32-native-ref s (fx+ ss i)))))
          ((and (fx=? (fxand #b11 ss) (fxand #b11 ts)) (fx>? k 4))
           ;; Align the indices to enable u32 operations
           (let ((a (fx- 4 (fxand #b11 ts))))
             (do ((i 0 (fx+ i 1)))
                 ((fx=? i a))
               (bytevector-u8-set! t (fx+ ts i)
                                   (bytevector-u8-ref s (fx+ ss i))))
             (copy! (fx+ ss a) (fx+ ts a) (fx- k a))))
          ((eqv? 0 (fxand #b1 (fxior ss ts)))
           (do ((k^ (fxand k -2))
                (i 0 (fx+ i 2)))
               ((fx=? i k^)
                (do ((i i (fx+ i 1)))
                    ((fx=? i k))
                  (bytevector-u8-set! t (fx+ ts i)
                                      (bytevector-u8-ref s (fx+ ss i)))))
             (bytevector-u16-native-set! t (fx+ ts i)
                                         (bytevector-u16-native-ref s (fx+ ss i)))))
          (else
           (do ((i 0 (fx+ i 1)))
               ((fx=? i k))
             (bytevector-u8-set! t (fx+ ts i)
                                 (bytevector-u8-ref s (fx+ ss i))))))))

(define (bytevector-copy bv)
  (let ((ret (make-bytevector (bytevector-length bv))))
    (bytevector-copy! bv 0 ret 0 (bytevector-length bv))
    ret))

;;; Bytes and octets

;; XXX: unless these procedures read the length, they will not
;; trigger an alignment check.

(define (bytevector-u8-ref x i) (sys:bytevector-u8-ref x i))

(define (bytevector-s8-ref x i) (sys:bytevector-s8-ref x i))

(define (bytevector-u8-set! x i v) (sys:bytevector-u8-set! x i v))

(define (bytevector-s8-set! x i v) (sys:bytevector-s8-set! x i v))

(define (bytevector->u8-list bv)
  (when (not (bytevector? bv))
    (assertion-violation 'bytevector->u8-list
                         "Expected a bytevector" bv))
  (do ((i (fx- (bytevector-length bv) 1) (fx- i 1))
       (ret '() (cons (bytevector-u8-ref bv i) ret)))
      ((fx=? i -1) ret)))

(define (u8-list->bytevector list)
  ;; XXX: length checks for improper lists, but will report the
  ;; wrong &who condition. TODO: add a length/f procedure?
  (let ((len (length list)))
    (do ((ret (make-bytevector len))
         (i 0 (fx+ i 1))
         (u8s list (cdr u8s)))
        ((fx=? i len) ret)
      (let ((o (car u8s)))
        (when (not (and (fixnum? o) (fx<=? 0 o 255)))
          (assertion-violation 'u8-list->bytevector
                               "Expected a list of octets" list))
        (bytevector-u8-set! ret i o)))))

;;; Integers of arbitrary size

(define (bytevector-uint-ref bv k endian size)
  ;; TODO: Read more bits per loop iteration.
  (assert (fxpositive? size))
  (assert (fx<=? 0 k (fx- (bytevector-length bv) size)))
  (case endian
    ((little)
     (do ((end (fx- k 1))
          (i (fx+ k (fx- size 1)) (fx- i 1))
          (ret 0 (bitwise-ior (bitwise-arithmetic-shift-left ret 8)
                              (bytevector-u8-ref bv i))))
         ((fx=? i end)
          ret)))
    ((big)
     (do ((end (fx+ k size))
          (i k (fx+ i 1))
          (ret 0 (bitwise-ior (bitwise-arithmetic-shift-left ret 8)
                              (bytevector-u8-ref bv i))))
         ((fx=? i end)
          ret)))
    (else
     (assertion-violation 'bytevector-uint-ref
                          "Unsupported endianness"
                          bv k endian size))))

(define (bytevector-sint-ref bv k endian size)
  (assert (fxpositive? size))
  (assert (fx<=? 0 k (fx- (bytevector-length bv) size)))
  (case endian
    ((little)
     (do ((end (fx- k 1))
          (i (fx+ k (fx- size 2)) (fx- i 1))
          (ret (bytevector-s8-ref bv (fx+ k (fx- size 1)))
               (bitwise-ior (bitwise-arithmetic-shift-left ret 8)
                            (bytevector-u8-ref bv i))))
         ((fx=? i end) ret)))
    ((big)
     (do ((end (fx+ k size))
          (i (fx+ k 1) (fx+ i 1))
          (ret (bytevector-s8-ref bv k)
               (bitwise-ior (bitwise-arithmetic-shift-left ret 8)
                            (bytevector-u8-ref bv i))))
         ((fx=? i end) ret)))
    (else
     (assertion-violation 'bytevector-uint-ref "Unsupported endianness"
                          bv k endian size))))

(define (bytevector-uint-set! bv k n^ endian size)
  (assert (fxpositive? size))
  (assert (fx<=? 0 k (fx- (bytevector-length bv) size)))
  (case endian
    ((little)
     (do ((i k (fx+ i 1))
          (end (fx+ k (fx- size 1)))
          (n n^ (bitwise-arithmetic-shift-right n 8)))
         ((fx=? i end)
          (unless (fx<=? 0 n 255)
            (assertion-violation 'bytevector-uint-set! "Integer out of range"
                                 bv k n^ endian size))
          (bytevector-u8-set! bv i n))
       (bytevector-u8-set! bv i (bitwise-and n #xff))))
    ((big)
     (do ((end k)
          (i (fx+ k (fx- size 1)) (fx- i 1))
          (n n^ (bitwise-arithmetic-shift-right n 8)))
         ((fx=? i end)
          (unless (fx<=? 0 n 255)
            (assertion-violation 'bytevector-uint-set! "Integer out of range"
                                 bv k n^ endian size))
          (bytevector-u8-set! bv i n))
       (bytevector-u8-set! bv i (bitwise-and n #xff))))
    (else
     (assertion-violation 'bytevector-uint-set! "Unsupported endianness"
                          bv k endian size))))

(define (bytevector-sint-set! bv k n^ endian size)
  (assert (fxpositive? size))
  (assert (fx<=? 0 k (fx- (bytevector-length bv) size)))
  (case endian
    ((little)
     (do ((end (fx+ k (fx- size 1)))
          (i k (fx+ i 1))
          (n n^ (bitwise-arithmetic-shift-right n 8)))
         ((fx=? i end)
          (unless (fx<=? -128 n 127)
            (assertion-violation 'bytevector-sint-set! "Integer out of range"
                                 bv k n^ endian size))
          (bytevector-s8-set! bv i n))
       (bytevector-u8-set! bv i (bitwise-and n #xff))))
    ((big)
     (do ((end k)
          (i (fx+ k (fx- size 1)) (fx- i 1))
          (n n^ (bitwise-arithmetic-shift-right n 8)))
         ((fx=? i end)
          (unless (fx<=? -128 n 127)
            (assertion-violation 'bytevector-sint-set! "Integer out of range"
                                 bv k n^ endian size))
          (bytevector-s8-set! bv i n))
       (bytevector-u8-set! bv i (bitwise-and n #xff))))
    (else
     (assertion-violation 'bytevector-sint-set!
                          "Unsupported endianness"
                          bv k endian size))))

(define (bytevector->uint-list bv endian size)
  (unless (eqv? 0 (fxmod (bytevector-length bv) size))
    (assertion-violation 'bytevector->uint-list
                         "Bytevector length must be a multiple of the size"
                         bv endian size))
  (case size
    ((1)
     (do ((i (fx- (bytevector-length bv) 1) (fx- i 1))
          (ret '() (cons (bytevector-u8-ref bv i endian) ret)))
         ((fx<? i 0) ret)))
    ((2)
     (do ((i (fx- (bytevector-length bv) 2) (fx- i 2))
          (ret '() (cons (bytevector-u16-ref bv i endian) ret)))
         ((fx<? i 0) ret)))
    ((4)
     (do ((i (fx- (bytevector-length bv) 4) (fx- i 4))
          (ret '() (cons (bytevector-u32-ref bv i endian) ret)))
         ((fx<? i 0) ret)))
    ((8)
     (do ((i (fx- (bytevector-length bv) 8) (fx- i 8))
          (ret '() (cons (bytevector-u64-ref bv i endian) ret)))
         ((fx<? i 0) ret)))
    (else
     (do ((i (fx- (bytevector-length bv) size) (fx- i size))
          (ret '() (cons (bytevector-uint-ref bv i endian size) ret)))
         ((fx<? i 0) ret)))))

(define (bytevector->sint-list bv endian size)
  (unless (eqv? 0 (fxmod (bytevector-length bv) size))
    (assertion-violation 'bytevector->sint-list
                         "Bytevector length must be a multiple of the size"
                         bv endian size))
  (case size
    ((1)
     (do ((i (fx- (bytevector-length bv) 1) (fx- i 1))
          (ret '() (cons (bytevector-s8-ref bv i endian) ret)))
         ((fx<? i 0) ret)))
    ((2)
     (do ((i (fx- (bytevector-length bv) 2) (fx- i 2))
          (ret '() (cons (bytevector-s16-ref bv i endian) ret)))
         ((fx<? i 0) ret)))
    ((4)
     (do ((i (fx- (bytevector-length bv) 4) (fx- i 4))
          (ret '() (cons (bytevector-s32-ref bv i endian) ret)))
         ((fx<? i 0) ret)))
    ((8)
     (do ((i (fx- (bytevector-length bv) 8) (fx- i 8))
          (ret '() (cons (bytevector-s64-ref bv i endian) ret)))
         ((fx<? i 0) ret)))
    (else
     (do ((i (fx- (bytevector-length bv) size) (fx- i size))
          (ret '() (cons (bytevector-sint-ref bv i endian size) ret)))
         ((fx<? i 0) ret)))))

(define (uint-list->bytevector list endian size)
  (let ((ret (make-bytevector (fx* size (length list)))))
    (do ((k 0 (fx+ k size))
         (list list (cdr list)))
        ((null? list) ret)
      (bytevector-uint-set! ret k (car list) endian size))))

(define (sint-list->bytevector list endian size)
  (let ((ret (make-bytevector (fx* size (length list)))))
    (do ((k 0 (fx+ k size))
         (list list (cdr list)))
        ((null? list) ret)
      (bytevector-sint-set! ret k (car list) endian size))))

;;; 16-bit integers

(define (bytevector-u16-ref bv idx endian)
  (define (wrong)
    (assertion-violation 'bytevector-u16-ref
                         "Unsupported endianness"
                         bv idx endian))
  (if (opencoded? ref u16)
      (case endian
        ((little) (sys:bytevector-u16-ref bv idx (endianness little)))
        ((big) (sys:bytevector-u16-ref bv idx (endianness big)))
        (else (wrong)))
      (let ((b2 (bytevector-u8-ref bv (fx+ idx 1)))
            (b1 (bytevector-u8-ref bv idx)))
        (case endian
          ((little)
           (fxior (fxarithmetic-shift-left b2 8) b1))
          ((big)
           (fxior (fxarithmetic-shift-left b1 8) b2))
          (else (wrong))))))

(define (bytevector-s16-ref bv idx endian)
  (define (wrong)
    (assertion-violation 'bytevector-s16-ref
                         "Unsupported endianness"
                         bv idx endian))
  (if (opencoded? ref s16)
      (case endian
        ((little) (sys:bytevector-s16-ref bv idx (endianness little)))
        ((big) (sys:bytevector-s16-ref bv idx (endianness big)))
        (else (wrong)))
      (case endian
        ((little)
         (let ((b2 (bytevector-s8-ref bv (fx+ idx 1)))
               (b1 (bytevector-u8-ref bv idx)))
           (fxior (fxarithmetic-shift-left b2 8) b1)))
        ((big)
         (let ((b2 (bytevector-u8-ref bv (fx+ idx 1)))
               (b1 (bytevector-s8-ref bv idx)))
           (fxior (fxarithmetic-shift-left b1 8) b2)))
        (else (wrong)))))

(define (bytevector-u16-native-ref bv i) (sys:bytevector-u16-native-ref bv i))

(define (bytevector-s16-native-ref bv i) (sys:bytevector-s16-native-ref bv i))

(define (bytevector-u16-set! bv idx v endianness)
  (assert (fx<=? 0 v #xffff))
  (case endianness
    ((little)
     (bytevector-u8-set! bv idx (fxand v #xff))
     (bytevector-u8-set! bv (fx+ idx 1) (fxarithmetic-shift-right v 8)))
    ((big)
     (bytevector-u8-set! bv idx (fxarithmetic-shift-right v 8))
     (bytevector-u8-set! bv (fx+ idx 1) (fxand v #xff)))
    (else
     (assertion-violation 'bytevector-u16-set!
                          "Unsupported endianness."
                          bv idx v endianness))))

(define (bytevector-s16-set! bv idx v endian)
  (assert (fx<=? (- (expt 2 15)) v (- (expt 2 15) 1)))
  (bytevector-u16-set! bv idx (fxand v #xffff) endian))

(define (bytevector-u16-native-set! bv i v)
  (sys:bytevector-u16-native-set! bv i v))

(define (bytevector-s16-native-set! bv i v)
  (sys:bytevector-s16-native-set! bv i v))

;;; 32-bit integers

(define (bytevector-u32-ref bv idx endian)
  (define (wrong)
    (assertion-violation 'bytevector-u32-ref
                         "Unsupported endianness."
                         bv idx endian))
  (if (opencoded? ref u32)
      (case endian
        ((little) (sys:bytevector-u32-ref bv idx (endianness little)))
        ((big) (sys:bytevector-u32-ref bv idx (endianness big)))
        (else (wrong)))
      (let ((b4 (bytevector-u8-ref bv (fx+ idx 3)))
            (b3 (bytevector-u8-ref bv (fx+ idx 2)))
            (b2 (bytevector-u8-ref bv (fx+ idx 1)))
            (b1 (bytevector-u8-ref bv idx)))
        (case endian
          ((little)
           (fxior (fxarithmetic-shift-left b4 24)
                  (fxarithmetic-shift-left b3 16)
                  (fxarithmetic-shift-left b2 8)
                  b1))
          ((big)
           (fxior (fxarithmetic-shift-left b1 24)
                  (fxarithmetic-shift-left b2 16)
                  (fxarithmetic-shift-left b3 8)
                  b4))
          (else (wrong))))))

(define (bytevector-s32-ref bv idx endian)
  (define (wrong)
    (assertion-violation 'bytevector-s32-ref
                         "Unsupported endianness."
                         bv idx endian))
  (if (opencoded? ref u32)
      (case endian
        ((little) (sys:bytevector-s32-ref bv idx (endianness little)))
        ((big) (sys:bytevector-s32-ref bv idx (endianness big)))
        (else (wrong)))
      (case endian
        ((little)
         (let ((b4 (bytevector-s8-ref bv (fx+ idx 3)))
               (b3 (bytevector-u8-ref bv (fx+ idx 2)))
               (b2 (bytevector-u8-ref bv (fx+ idx 1)))
               (b1 (bytevector-u8-ref bv idx)))
           (fxior (fxarithmetic-shift-left b4 24)
                  (fxarithmetic-shift-left b3 16)
                  (fxarithmetic-shift-left b2 8)
                  b1)))
        ((big)
         (let ((b4 (bytevector-u8-ref bv (fx+ idx 3)))
               (b3 (bytevector-u8-ref bv (fx+ idx 2)))
               (b2 (bytevector-u8-ref bv (fx+ idx 1)))
               (b1 (bytevector-s8-ref bv idx)))
           (fxior (fxarithmetic-shift-left b1 24)
                  (fxarithmetic-shift-left b2 16)
                  (fxarithmetic-shift-left b3 8)
                  b4)))
        (else (wrong)))))

(define (bytevector-u32-native-ref bv i) (sys:bytevector-u32-native-ref bv i))

(define (bytevector-s32-native-ref bv i) (sys:bytevector-s32-native-ref bv i))

(define (bytevector-u32-set! bv idx v endian)
  (assert (fx<=? 0 v #xffffffff))
  (case endian
    ((little)
     (bytevector-u16-set! bv idx (bitwise-and v #xffff)
                          (endianness little))
     (bytevector-u16-set! bv (fx+ idx 2)
                          (bitwise-arithmetic-shift-right v 16)
                          (endianness little)))
    ((big)
     (bytevector-u16-set! bv idx (bitwise-arithmetic-shift-right v 16)
                          (endianness big))
     (bytevector-u16-set! bv (fx+ idx 2)
                          (bitwise-and v #xffff)
                          (endianness big)))
    (else
     (assertion-violation 'bytevector-u32-set!
                          "Unsupported endianness."
                          bv idx v endian))))

(define (bytevector-s32-set! bv idx v endian)
  (assert (<= (- (expt 2 31)) v (- (expt 2 31) 1)))
  (bytevector-u32-set! bv idx (bitwise-and v #xffffffff) endian))

(define (bytevector-u32-native-set! bv i v)
  (sys:bytevector-u32-native-set! bv i v))

(define (bytevector-s32-native-set! bv i v)
  (sys:bytevector-s32-native-set! bv i v))

;;; 64-bit integers

(define (bytevector-u64-ref bv idx endian)
  (case endian
    ((little)
     (let ((dw1 (bytevector-u32-ref bv (fx+ idx 4) (endianness little)))
           (dw0 (bytevector-u32-ref bv idx (endianness little))))
       (bitwise-ior (bitwise-arithmetic-shift-left dw1 32)
                    dw0)))
    ((big)
     (let ((dw1 (bytevector-u32-ref bv (fx+ idx 4) (endianness big)))
           (dw0 (bytevector-u32-ref bv idx (endianness big))))
       (bitwise-ior (bitwise-arithmetic-shift-left dw0 32)
                    dw1)))
    (else
     (assertion-violation 'bytevector-u64-ref
                          "Unsupported endianness."
                          bv idx endian))))

(define (bytevector-s64-ref bv idx endian)
  (case endian
    ((little)
     (let ((dw1 (bytevector-s32-ref bv (fx+ idx 4) (endianness little)))
           (dw0 (bytevector-u32-ref bv idx (endianness little))))
       (bitwise-ior (bitwise-arithmetic-shift-left dw1 32)
                    dw0)))
    ((big)
     (let ((dw1 (bytevector-u32-ref bv (fx+ idx 4) (endianness big)))
           (dw0 (bytevector-s32-ref bv idx (endianness big))))
       (bitwise-ior (bitwise-arithmetic-shift-left dw0 32)
                    dw1)))
    (else
     (assertion-violation 'bytevector-u64-ref
                          "Unsupported endianness"
                          bv idx endian))))

(define (bytevector-u64-native-ref bv idx)
  (assert (fxzero? (fxand idx #b111)))
  (bytevector-u64-ref bv idx (native-endianness)))

(define (bytevector-s64-native-ref bv idx)
  (assert (fxzero? (fxand idx #b111)))
  (bytevector-s64-ref bv idx (native-endianness)))

(define (bytevector-u64-set! bv idx v endian)
  (assert (<= 0 v #xffffffffffffffff))
  (case endian
    ((little)
     (bytevector-u32-set! bv idx
                          (bitwise-and v #xffffffff)
                          (endianness little))
     (bytevector-u32-set! bv (fx+ idx 4)
                          (bitwise-arithmetic-shift-right v 32)
                          (endianness little)))
    ((big)
     (bytevector-u32-set! bv idx
                          (bitwise-arithmetic-shift-right v 32)
                          (endianness big))
     (bytevector-u32-set! bv (fx+ idx 4)
                          (bitwise-and v #xffffffff)
                          (endianness big)))
    (else
     (assertion-violation 'bytevector-u64-set!
                          "Unsupported endianness"
                          bv idx v endian))))

(define (bytevector-s64-set! bv idx v endian)
  (assert (<= (- (expt 2 63)) v (- (expt 2 63) 1)))
  (bytevector-u64-set! bv idx (bitwise-and v #xffffffffffffffff) endian))

(define (bytevector-u64-native-set! bv idx v)
  (assert (fxzero? (fxand idx #b111)))
  (bytevector-u64-set! bv idx v (native-endianness)))

(define (bytevector-s64-native-set! bv idx v)
  (assert (fxzero? (fxand idx #b111)))
  (bytevector-s64-set! bv idx v (native-endianness)))

;;; IEEE-754 representations

(define (bytevector-ieee-single-native-ref bv idx)
  (sys:bytevector-ieee-single-native-ref bv idx))

(define (bytevector-ieee-single-ref bv idx endian)
  (let ((tmp (make-bytevector 4)))
    (bytevector-u32-native-set! tmp 0 (bytevector-u32-ref bv idx endian))
    (bytevector-ieee-single-native-ref tmp 0)))

(define (bytevector-ieee-double-native-ref bv idx)
  (sys:bytevector-ieee-double-native-ref bv idx))

(define (bytevector-ieee-double-ref bv idx endian)
  (let ((tmp (make-bytevector 8)))
    (bytevector-u64-native-set! tmp 0 (bytevector-u64-ref bv idx endian))
    (bytevector-ieee-double-native-ref tmp 0)))

(define (bytevector-ieee-single-native-set! bv idx v)
  (sys:bytevector-ieee-single-native-set! bv idx v))

(define (bytevector-ieee-single-set! bv idx v endian)
  (let ((tmp (make-bytevector 4)))
    (bytevector-ieee-single-native-set! tmp 0 v)
    (bytevector-u32-set! bv idx (bytevector-u32-native-ref tmp 0) endian)))

(define (bytevector-ieee-double-native-set! bv idx v)
  (sys:bytevector-ieee-double-native-set! bv idx v))

(define (bytevector-ieee-double-set! bv idx v endian)
  (let ((tmp (make-bytevector 8)))
    (bytevector-ieee-double-native-set! tmp 0 v)
    (bytevector-u64-set! bv idx (bytevector-u64-native-ref tmp 0) endian)))

;;; Strings

(define (string->utf8 x)
  (call-with-bytevector-output-port
    (lambda (p) (put-string p x))
    (native-transcoder)))

(define string->utf16
  (case-lambda
    ((x)
     (string->utf16 x (endianness big)))
    ((x endian)
     (unless (memq endian '(big little))
       (assertion-violation 'string->utf16 "Unsupported endianness" x endian))
     (let* ((len (do ((i (fx- (string-length x) 1) (fx- i 1))
                      (slen 0 (if (fx<? (char->integer (string-ref x i)) #x10000)
                                  (fx+ slen 2) (fx+ slen 4))))
                     ((eqv? i -1) slen)))
            (ret (make-bytevector len)))
       (let lp ((si 0) (bi 0))
         (unless (fx=? bi len)
           (let ((cp (char->integer (string-ref x si))))
             (cond
               ((fx<? cp #x10000)
                (bytevector-u16-set! ret bi cp endian)
                (lp (fx+ si 1) (fx+ bi 2)))
               (else
                (let ((cp^ (fx- cp #x10000)))
                  (let ((high (fx+ #xD800 (fxarithmetic-shift-right cp^ 10)))
                        (low (fx+ #xDC00 (fxbit-field cp^ 0 10))))
                    (bytevector-u16-set! ret bi high endian)
                    (bytevector-u16-set! ret (fx+ bi 2) low endian)))
                (lp (fx+ si 1) (fx+ bi 4)))))))
       ret))))

(define string->utf32
  (case-lambda
    ((x)
     (string->utf32 x (endianness big)))
    ((x endian)
     (let* ((len (string-length x))
            (ret (make-bytevector (fx* 4 len))))
       (case endian
         ((big)
          (do ((i 0 (fx+ i 1)))
              ((fx=? i len))
            (bytevector-u32-set! ret (fx* i 4) (char->integer (string-ref x i))
                                 (endianness big))))
         ((little)
          (do ((i 0 (fx+ i 1)))
              ((fx=? i len))
            (bytevector-u32-set! ret (fx* i 4) (char->integer (string-ref x i))
                                 (endianness little))))
         (else
          (assertion-violation 'string->utf32 "Unsupported endianness" x endian)))
       ret))))

(define (utf8->string x)
  ;; XXX: assumes the native transcoder uses: utf-8-codec, none, replace
  (let ((str (get-string-all (transcoded-port (open-bytevector-input-port x)
                                              (native-transcoder)))))
    (if (eof-object? str) "" str)))

(define utf16->string
  (case-lambda
    ((bv endian)
     (utf16->string bv endian #f))
    ((bv endian endianness-mandatory?)
     (call-with-string-output-port
       (lambda (p)
         (define (put c)
           (put-char p c))
         (let* ((BOM (if (fx<? (bytevector-length bv) 2)
                         #f
                         (let ((bom (bytevector-u16-ref bv 0 (endianness big))))
                           (cond ((fx=? bom #xFEFF) (endianness big))
                                 ((fx=? bom #xFFFE) (endianness little))
                                 (else #f)))))
                (endian (if endianness-mandatory? endian (or BOM endian)))
                (skip (if (and BOM (not endianness-mandatory?)) 2 0)))
           (let lp ((i skip)
                    (rem (fx- (bytevector-length bv) skip)))
             (cond
               ((eqv? rem 0))
               ((eqv? rem 1) (put #\xFFFD))
               (else
                (let ((w0 (bytevector-u16-ref bv i endian))
                      (i^ (fx+ i 2))
                      (rem^ (fx- rem 2)))
                  (cond
                    ((fx<=? #xD800 w0 #xDFFF)
                     ;; Surrogate pair
                     (cond
                       ((fx<? rem^ 2)
                        (put #\xFFFD)
                        (lp i^ rem^))
                       (else
                        ;; Interesting: the ordering of the
                        ;; surrogate pairs forms a second level of
                        ;; endianness, which thankfully is fixed as
                        ;; big endian.
                        (let ((w1 (bytevector-u16-ref bv i^ endian))
                              (i^^ (fx+ i^ 2))
                              (rem^^ (fx- rem^ 2)))
                          (cond ((fx<=? #xD800 w1 #xDFFF)
                                 (let ((w (fx+ (fxior (fxarithmetic-shift-left (fx- w0 #xD800) 10)
                                                      (fxbit-field (fx- w1 #xDC00) 0 10))
                                               #x10000)))
                                   (cond ((fx>? w #x10FFFF)
                                          (put #\xFFFD)
                                          (lp i^ rem^))
                                         (else
                                          (put (integer->char w))
                                          (lp i^^ rem^^)))))
                                (else
                                 (put #\xFFFD)
                                 (lp i^ rem^)))))))
                    (else
                     (put (integer->char w0))
                     (lp i^ rem^)))))))))))))

(define utf32->string
  (case-lambda
    ((bv endian)
     (utf32->string bv endian #f))
    ((bv endian endianness-mandatory?)
     (call-with-string-output-port
       (lambda (p)
         (define (put c)
           (put-char p c))
         (let* ((BOM (if (fx<? (bytevector-length bv) 4)
                         #f
                         (let ((bom (bytevector-u32-ref bv 0 (endianness big))))
                           (cond ((eqv? bom #x0000FEFF) (endianness big))
                                 ((eqv? bom #xFFFE0000) (endianness little))
                                 (else #f)))))
                (endian (if endianness-mandatory? endian (or BOM endian)))
                (skip (if (and BOM (not endianness-mandatory?)) 4 0)))
           (let lp ((i skip)
                    (rem (fx- (bytevector-length bv) skip)))
             (cond
               ((eqv? rem 0))
               ((fx<? rem 4)
                (put #\xFFFD))
               (else
                (let ((w0 (bytevector-u32-ref bv i endian))
                      (i^ (fx+ i 4))
                      (rem^ (fx- rem 4)))
                  (cond
                    ((or (fx<=? #xD800 w0 #xDFFF)
                         (fx>? w0 #x10FFFF))
                     ;; Surrogate pair or out of range
                     (put #\xFFFD)
                     (lp i^ rem^))
                    (else
                     (put (integer->char w0))
                     (lp i^ rem^)))))))))))))

(define (bytevector-address bv)
  (sys:bytevector-address bv)))
