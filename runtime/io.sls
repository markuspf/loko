;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: AGPL-3.0-or-later
;; Loko Scheme - an R6RS Scheme compiler
;; Copyright © 2019-2020 Göran Weinholt

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
#!r6rs

;;; Scheme I/O ports

;; There are tricky dependencies at startup related to ports.

;; Buffering combinations:
;; input/none: read one byte at a time
;; input/block: try to fill the buffer
;; input/line: same as block. tty line discipline provides line buffering.
;; output/none: pass each u8 or bytevector in one go. put-char puts whole char.
;; output/block: try to fill and send the whole buffer
;; output/line: try to fill the buffer until there's a linefeed

(library (loko runtime io)
  (export
    buffer-mode? latin-1-codec utf-8-codec utf-16-codec
    native-eol-style

    make-transcoder native-transcoder
    transcoder-codec transcoder-eol-style
    transcoder-error-handling-mode
    bytevector->string string->bytevector

    eof-object eof-object?

    port? port-transcoder textual-port? binary-port?
    transcoded-port port-has-port-position?
    port-position port-has-set-port-position!?
    set-port-position! close-port
    call-with-port

    input-port? port-eof? open-bytevector-input-port
    open-string-input-port standard-input-port current-input-port
    make-custom-binary-input-port make-custom-textual-input-port
    get-u8 lookahead-u8 get-bytevector-n get-bytevector-n!
    get-bytevector-some get-bytevector-all
    get-char lookahead-char get-string-n get-string-n! get-string-all
    get-line
    output-port? flush-output-port output-port-buffer-mode
    open-bytevector-output-port
    call-with-bytevector-output-port open-string-output-port
    call-with-string-output-port standard-output-port
    standard-error-port current-output-port current-error-port
    make-custom-binary-output-port make-custom-textual-output-port
    put-u8 put-bytevector put-char put-string put-datum
    make-custom-binary-input/output-port make-custom-textual-input/output-port
    call-with-input-file call-with-output-file with-input-from-file
    with-output-to-file open-input-file open-output-file
    close-input-port close-output-port read-char peek-char
    write-char newline display write

    find-cycles                         ;temporarily for pretty-print
    make-file-options                   ;for psyntax/expander
    $port-buffer-mode-set!              ;for various open-file-stuff
    $init-standard-ports
    open-output-string
    get-output-string
    port-file-descriptor
    port-file-descriptor-set!
    port-buffer-mode-set!
    port-reader
    port-reader-set!
    port-id
    add-fdes-finalizer!
    call-fd-finalizer
    input-port-open? output-port-open?
    write-shared
    write-simple
    print-dialects
    current-output-port*
    current-error-port*
    current-input-port*
    u8-ready?
    char-ready?)
  (import
    (except (rnrs)
            buffer-mode? latin-1-codec utf-8-codec utf-16-codec
            native-eol-style
            make-transcoder native-transcoder
            transcoder-codec transcoder-eol-style
            transcoder-error-handling-mode
            bytevector->string string->bytevector
            eof-object eof-object?
            port? port-transcoder textual-port? binary-port?
            transcoded-port port-has-port-position?
            port-position port-has-set-port-position!?
            set-port-position! close-port
            call-with-port
            input-port? port-eof? open-bytevector-input-port
            open-string-input-port standard-input-port current-input-port
            make-custom-binary-input-port make-custom-textual-input-port
            get-u8 lookahead-u8 get-bytevector-n get-bytevector-n!
            get-bytevector-some get-bytevector-all
            get-char lookahead-char get-string-n get-string-n! get-string-all
            get-line get-datum
            output-port? flush-output-port output-port-buffer-mode
            open-bytevector-output-port
            call-with-bytevector-output-port open-string-output-port
            call-with-string-output-port standard-output-port
            standard-error-port current-output-port current-error-port
            make-custom-binary-output-port make-custom-textual-output-port
            put-u8 put-bytevector put-char put-string put-datum
            make-custom-binary-input/output-port
            make-custom-textual-input/output-port
            call-with-input-file call-with-output-file with-input-from-file
            with-output-to-file open-input-file open-output-file
            close-input-port close-output-port read-char peek-char read
            write-char newline display write)
    (only (rnrs mutable-strings) string-set!)
    (rnrs mutable-pairs)
    (prefix (rnrs io ports) sys:)
    (loko runtime io-tc)
    (only (loko runtime arithmetic) $display-number)
    (only (loko runtime symbols) $gensym-generate-names!)
    (only (loko runtime records) record-writer)
    (only (loko system fibers) make-cvar signal-cvar! wait)
    (only (loko) make-parameter parameterize)
    (loko system $primitives)
    (loko compiler compat)
    (loko system fibers))

;; Tracing this only works when $debug-put-u8 is used directly.
(define-syntax trace
  (syntax-rules ()
    #;
    ((_ . args)
     (begin
       (for-each display (list . args))
       (newline)))
    ((_ . args) 'dummy)))

;; The `file-options' macro residualizes a call to make-file-options
(define file-options-set (make-enumeration '(no-create no-fail no-truncate)))
(define make-file-options (enum-set-constructor file-options-set))

(define (buffer-mode? obj)
  (and (memq obj '(none line block)) #t))

(define (latin-1-codec) 'latin-1-codec)

(define (utf-8-codec) 'utf-8-codec)

(define (utf-16-codec) 'utf-16-codec)

(define (native-eol-style) 'lf)         ;Unix-style

(define-record-type transcoder
  (fields codec eol-style error-handling-mode)
  (sealed #t)
  (opaque #f)
  (nongenerative transcoder-56083a04-ec39-49a3-9a4e-a1891ed48f72)
  (protocol
   (lambda (p)
     (define make-transcoder
       (case-lambda
         ((c) (make-transcoder c (native-eol-style) (error-handling-mode replace)))
         ((c e) (make-transcoder c e (error-handling-mode replace)))
         ((c e h)
          ;; codec eol-style handling-mode
          (assert (memq c '(utf-8-codec latin-1-codec utf-16-codec)))
          (assert (memq e '(none lf crlf cr nel crnel ls)))
          (assert (memq h '(replace ignore raise)))
          (p c e h))))
     make-transcoder)))

(define %native-transcoder
  (make-transcoder (utf-8-codec)
                   (eol-style none)
                   (error-handling-mode replace)))

(define (native-transcoder)
  %native-transcoder)

(define (bytevector->string bytevector transcoder)
  (get-string-all (open-bytevector-input-port bytevector transcoder)))

(define (string->bytevector string transcoder)
  (let-values ([(p extract) (open-bytevector-output-port transcoder)])
    (put-string p string)
    (flush-output-port p)
    (extract)))

(define (eof-object) (sys:eof-object))

(define (eof-object? x) (sys:eof-object? x))

;; File descriptor finalizers. Used to do something special when an fd
;; is closed.
(define *finalizers* (make-eqv-hashtable))

(define (add-fdes-finalizer! fdes finalizer)
  (hashtable-update! *finalizers* fdes
                     (lambda (old)
                       (cons finalizer old))
                     '()))

(define (call-fd-finalizer fd)
  (cond ((hashtable-ref *finalizers* fd #f)
         => (lambda (finalizer*)
              (hashtable-delete! *finalizers* fd)
              (for-each (lambda (finalizer)
                          (finalizer fd))
                        finalizer*)))))

(define print-dialects
  (make-parameter '()
                  (lambda (new)
                    (assert (for-all symbol? new))
                    new)))

;;; Port data type

(define PORT-DIR-OUTPUT   #b00001)
(define PORT-DIR-INPUT    #b00010)
(define PORT-TYPE-TEXTUAL #b00100)

(define BUFFER-MODE-MASK  #b11000)
(define BUFFER-MODE-NONE  #b00000)
(define BUFFER-MODE-LINE  #b01000)
(define BUFFER-MODE-BLOCK #b10000)

(define-syntax define-box-type
  (lambda (x)
    (define (symcat . x*)
      (string->symbol
       (apply string-append
              (map (lambda (x)
                     (let ((x (syntax->datum x)))
                       (if (string? x)
                           x
                           (symbol->string x))))
                   x*))))
    (define (iota n)
      (do ((n n (- n 1))
           (x* '() (cons (- n 1) x*)))
          ((eqv? n 0) x*)))
    (define (mkname prefix name fname suffix)
      (datum->syntax name
                     (symcat prefix (syntax->datum name) "-"
                             (syntax->datum fname) suffix)))
    (syntax-case x (fields mutable protocol)
      ((_ name
          (fields (mutable flagfield) field* ...)
          (protocol prot))
       (identifier? #'name)
       (with-syntax ((make (datum->syntax #'name (symcat "make-" (syntax->datum #'name))))
                     (pred (datum->syntax #'name (symcat (syntax->datum #'name) "?")))
                     ((arg* ...) (generate-temporaries #'(field* ...)))
                     ((idx* ...) (iota (length (syntax->datum #'(field* ...)))))
                     (len (length (syntax->datum #'(field* ...))))
                     (flagref (mkname "" #'name #'flagfield ""))
                     (flagset (mkname "" #'name #'flagfield "-set!")))
         (letrec ((expand-field
                   (lambda (field idx)
                     (syntax-case field (mutable)
                       [(mutable fname)
                        (identifier? #'fname)
                        (with-syntax ((ref (mkname "" #'name #'fname ""))
                                      (set (mkname "" #'name #'fname "-set!"))
                                      ;; Unchecked variants
                                      (uref (mkname "$" #'name #'fname ""))
                                      (uset (mkname "$" #'name #'fname "-set!")))
                          #`(begin
                              (define (ref b)
                                (assert ($box-header-type-eq? ($box-type b) 'name))
                                ($box-ref b #,idx))
                              (define (set b v)
                                (assert ($box-header-type-eq? ($box-type b) 'name))
                                ($box-set! b #,idx v))
                              (define (uref b) ($box-ref b #,idx))
                              (define (uset b v) ($box-set! b #,idx v))))]
                       [fname
                        (identifier? #'fname)
                        (with-syntax ((ref (mkname "" #'name #'fname "")))
                          #`(define (ref b)
                              (assert ($box-header-type-eq? ($box-type b) 'name))
                              ($box-ref b #,idx)))]))))
           #`(begin
               (define make
                 (prot (lambda (flagval arg* ...)
                         (let ((b ($make-box ($make-box-header 'name #t flagval len) len)))
                           ($box-set! b idx* arg*) ...
                           b))))
               (define (pred obj)
                 (and ($box? obj)
                      ($box-header-type-eq? ($box-type obj) 'name)))
               (define (flagref b)
                 (assert ($box-header-type-eq? ($box-type b) 'name))
                 ($box-header-value ($box-type b)))
               (define (flagset b v)
                 (assert ($box-header-type-eq? ($box-type b) 'name))
                 ($box-type-set! b ($make-box-header 'name #t v len)))
               #,@(map expand-field #'(field* ...) #'(idx* ...)))))))))

(define-box-type port
  (fields
   ;; Flags bits:
   ;; 0     1 = output port
   ;; 1     1 = input port (combined ports exist)
   ;; 2     1 = textual, 0 = binary
   ;; 3     \
   ;; 4     /  0=none, 1=line, 2=block
   (mutable flags)
   (mutable buffer)
   (mutable buffer-rpos)
   (mutable buffer-rend)
   (mutable buffer-wpos)
   (mutable buffer-wend)

   ;; Hooks
   (mutable sink)
   (mutable source)
   (mutable get-positioner)
   (mutable set-positioner)
   (mutable closer)
   (mutable extractor)                  ;for in-memory output ports

   id
   (mutable file-descriptor)            ;fd or #f
   transcoder
   (mutable nl)                         ;#\newline or #f
   (mutable cvar)
   (mutable reader)
   (mutable at-eof))
  (protocol
   (lambda (p)
     (lambda (buffer flags
                     sink source getpos setpos closer extractor
                     id transcoder)
       (assert (string? id))
       (assert (or sink source))
       (let ((buffer-rpos 0)
             (buffer-rend 0)
             (buffer-wpos 0)
             (buffer-wend
              (cond ((not sink)
                     0)
                    ((eqv? (fxand flags BUFFER-MODE-MASK) BUFFER-MODE-NONE)
                     0)
                    ((bytevector? buffer)
                     (bytevector-length buffer))
                    (else (string-length buffer))))
             (file-descriptor #f)
             (cvar #f)
             (nl (if (eqv? (fxand flags BUFFER-MODE-MASK) BUFFER-MODE-LINE)
                     #\newline
                     #f))
             (reader #f)
             (at-eof #f))
         (p flags buffer buffer-rpos buffer-rend buffer-wpos buffer-wend
            sink source getpos setpos closer extractor
            id file-descriptor transcoder nl cvar reader at-eof))))))

(define (port-buffer-mode p)
  (unless (port? p)
    (assertion-violation 'port-buffer-mode "Expected a port" p))
  (let ((mode (fxand BUFFER-MODE-MASK (port-flags p))))
    (cond ((eqv? mode BUFFER-MODE-NONE) 'none)
          ((eqv? mode BUFFER-MODE-LINE) 'line)
          (else 'block))))

(define ($port-buffer-mode-set! port mode)
  ;; It's the caller's responsibility to check that the mode is a
  ;; valid buffer-mode. The buffer mode can also only be changed once,
  ;; right after the port has been created.
  (port-flags-set! port (fxior (case mode
                                  ((none) BUFFER-MODE-NONE)
                                  ((line) BUFFER-MODE-LINE)
                                  (else BUFFER-MODE-BLOCK))
                                (fxand (fxnot BUFFER-MODE-MASK)
                                       (port-flags port))))
  (case mode
    ((line)
     (port-nl-set! port #\newline))
    ((none)
     ;; Make the buffer only hold one value. This way there's no need
     ;; to check if the buffer mode is none every time the port is
     ;; used.
     (port-buffer-wend-set! port 0)
     (if (string? (port-buffer port))
         (port-buffer-set! port (make-string 1))
         (port-buffer-set! port (make-bytevector 1))))))

(define (port-buffer-mode-set! port mode)
  (unless (port? port)
    (assertion-violation 'port-buffer-mode-set! "Expected a port" port mode))
  (unless (memq mode '(none line block))
    (assertion-violation 'port-buffer-mode-set! "Expected a buffer mode" port mode))
  ($port-buffer-mode-set! port mode))

(define (make-custom-binary-input-port id read! getpos setpos close)
  (make-port (make-bytevector 512) (fxior PORT-DIR-INPUT BUFFER-MODE-BLOCK)
             #f read! getpos setpos close #f
             id #f))

(define (make-custom-textual-input-port id read! getpos setpos close)
  (make-port (make-string 512) (fxior PORT-TYPE-TEXTUAL PORT-DIR-INPUT
                                      BUFFER-MODE-BLOCK)
             #f read! getpos setpos close #f
             id #f))

(define (make-custom-binary-output-port id write! getpos setpos close)
  (make-port (make-bytevector 512) (fxior PORT-DIR-OUTPUT BUFFER-MODE-BLOCK)
             write! #f getpos setpos close #f
             id #f))

(define (make-custom-textual-output-port id write! getpos setpos close)
  (make-port (make-string 512) (fxior PORT-TYPE-TEXTUAL PORT-DIR-OUTPUT
                                      BUFFER-MODE-BLOCK)
             write! #f getpos setpos close #f
             id #f))

;; input/output ports are unbuffered because the port would otherwise
;; need to be repositioned when going between reading and writing.
;; FIXME: just use separate read/write buffer capacities

(define (make-custom-binary-input/output-port id read! write! getpos setpos close)
  (make-port (make-bytevector 1) (fxior PORT-DIR-INPUT PORT-DIR-OUTPUT
                                        BUFFER-MODE-NONE)
             write! read! getpos setpos close #f
             id #f))

(define (make-custom-textual-input/output-port id read! write! getpos setpos close)
  (make-port (make-string 1) (fxior PORT-TYPE-TEXTUAL
                                    PORT-DIR-INPUT PORT-DIR-OUTPUT
                                    BUFFER-MODE-NONE)
             write! read! getpos setpos close #f
             id #f))

;; Make a new port object that mirrors exactly the original port. The
;; original port is closed. Used by transcoded-port: "transcoded-port
;; closes binary-port in a special way…".
;; FIXME: investigate if this is really needed
(define (clone/close-port p tc)
  (assert (port? p))
  (assert (port-buffer p))
  (let* ((ret (make-port (port-buffer p)

                         (port-flags p)
                         (port-sink p)
                         (port-source p)
                         (port-get-positioner p)
                         (port-set-positioner p)
                         (port-closer p)
                         (port-extractor p)

                         (port-id p)
                         tc)))
    (port-buffer-rpos-set! ret (port-buffer-rpos p))
    (port-buffer-rend-set! ret (port-buffer-rend p))
    (port-buffer-wpos-set! ret (port-buffer-wpos p))
    (port-file-descriptor-set! ret (port-file-descriptor p))
    (port-cvar-set! ret (port-cvar p))
    ;; Release the memory of the old port
    (port-sink-set! p #f)
    (port-source-set! p #f)
    (port-get-positioner-set! p #f)
    (port-set-positioner-set! p #f)
    (port-closer-set! p #f)
    (port-extractor-set! p #f)
    (port-file-descriptor-set! p #f)
    (port-buffer-set! p #f)
    (port-buffer-rpos-set! p 0)
    (port-buffer-rend-set! p 0)
    (port-buffer-wpos-set! p 0)
    (port-cvar-set! p #f)
    ret))

(define (transcoded-port binp tc)
  (unless (and (binary-port? binp) (port-buffer binp))
    (assertion-violation 'transcoded-port
                         "Expected an open binary port as the first argument" binp tc))
  (unless (transcoder? tc)
    (assertion-violation 'transcoded-port
                         "Expected a transcoder as the second argument" binp tc))
  (let* ((id (port-id binp))
         (binp (clone/close-port binp tc)))
    (let ((p (transcode-port id binp tc)))
      (port-file-descriptor-set! p (port-file-descriptor binp))
      ($port-buffer-mode-set! p (port-buffer-mode binp))
      p)))

;;; Predicates and general stuff

(define (textual-port? p)
  (unless (port? p)
    (assertion-violation 'textual-port? "Expected a port" p))
  (eqv? (fxand (port-flags p) #b100) #b100))

(define (binary-port? p)
  (unless (port? p)
    (assertion-violation 'binary-port? "Expected a port" p))
  (eqv? (fxand (port-flags p) #b100) 0))

(define (input-port? obj)
  #;(and (port? obj) (eqv? #b10 (fxand (port-flags obj) #b10)))
  (and ($box? obj) ($box-header-type-eq? ($box-type obj) 'port #b10 #b10)))

(define (output-port? obj)
  #;(and (port? obj) (eqv? #b01 (fxand (port-flags obj) #b01)))
  (and ($box? obj) ($box-header-type-eq? ($box-type obj) 'port #b01 #b01)))

(define (binary-input-port? obj)
  (and ($box? obj) ($box-header-type-eq? ($box-type obj) 'port #b110 #b010)))

(define (binary-output-port? obj)
  (and ($box? obj) ($box-header-type-eq? ($box-type obj) 'port #b101 #b001)))

(define (textual-input-port? obj)
  (and ($box? obj) ($box-header-type-eq? ($box-type obj) 'port #b110 #b110)))

(define (textual-output-port? obj)
  (and ($box? obj) ($box-header-type-eq? ($box-type obj) 'port #b101 #b101)))

(define (input-port-open? p)
  (and (port-source p) #t))

(define (output-port-open? p)
  (and (port-sink p) #t))

(define (port-has-port-position? p)
  (and (port-get-positioner p) #t))

(define (port-position p)
  (if (port-has-port-position? p)
      (let ((underlying-pos ((port-get-positioner p))))
        (cond ((not (eqv? 0 (port-buffer-rend p)))
               ;; In input mode the underlying object is at the
               ;; position immediately after the last byte/char in our
               ;; buffer.
               (- underlying-pos (fx- (port-buffer-rend p) (port-buffer-rpos p))))
              (else
               ;; In output mode the underlying object is positioned
               ;; at the immediate beginning of our buffer.
               (+ underlying-pos (port-buffer-wpos p)))))
      (assertion-violation 'port-position
                           "This port does not support port-position" p)))

(define (port-has-set-port-position!? p)
  (and (port-set-positioner p) #t))

(define (set-port-position! p pos)
  (assert (fx>=? pos 0))
  (cond
    ((port-set-positioner p) =>
     (lambda (setpos)
       (when (output-port? p)
         (flush-output-port p))
       (setpos pos)
       (port-buffer-rpos-set! p 0)
       (port-buffer-rend-set! p 0)
       (port-buffer-wpos-set! p 0)))
    (else
     (assertion-violation 'set-port-position!
                          "This port does not support set-port-position!" p))))

(define (close-port p)
  ;; TODO: it would be swell if ports with no references to them
  ;; also could close the affected sink/source (which might be a
  ;; file descriptor). The question is if the buffer should be
  ;; flushed in those cases. What language is it that flushes all
  ;; output ports on exit?
  (assert (port? p))
  (when (port-buffer p)
    (when (output-port? p)
      (flush-output-port p))
    (let ((closer (port-closer p)))
      (when closer
        (closer)))
    (port-sink-set! p #f)
    (port-source-set! p #f)
    (port-get-positioner-set! p #f)
    (port-set-positioner-set! p #f)
    (port-closer-set! p #f)
    (port-buffer-set! p #f)
    (port-buffer-rpos-set! p 0)
    (port-buffer-rend-set! p 0)
    (port-buffer-wpos-set! p 0))
  (values))

(define (call-with-port port proc)
  (assert (port? port))
  (let-values ((v (proc port)))
    (close-port port)
    (apply values v)))

;;; Input ports

(define (port-eof? p)
  (assert (input-port? p))
  ;; FIXME: what if error-mode is raise?
  (if (textual-port? p)
      (eof-object? (lookahead-char p))
      (eof-object? (lookahead-u8 p))))

;; open-file-input-port is defined elsewhere.

(define open-bytevector-input-port
  (case-lambda
    ((bv)
     (open-bytevector-input-port bv #f))
    ((bv tc)
     (define pos 0)
     (define (read! buf start count)
       (let ((n (fxmin (fx- (bytevector-length bv) pos)
                       count)))
         (bytevector-copy! bv pos buf start n)
         (set! pos (fx+ n pos))
         n))
     (define (get-position) pos)
     (define (set-position! new-pos) (set! pos new-pos))
     (define (close)
       (set! bv 'closed))
     ;; FIXME: should not need to copy the bv. it's for the position
     ;; stuff to work.
     (unless (bytevector? bv)
       (assertion-violation 'open-bytevector-input-port
                            "Expected a bytevector" bv tc))
     (let ((p (make-custom-binary-input-port
               "*bytevector*" read! get-position set-position! close)))
       (if tc (transcoded-port p tc) p)))))

(define (open-string-input-port str)
  (define pos 0)
  (define (read! buf start count)
    (let* ((str str)
           (n (fxmin (fx- (string-length str) pos)
                     count)))
      (do ((end (fx+ start n))
           (i start (fx+ i 1))
           (j pos (fx+ j 1)))
          ((fx=? i end)
           (set! pos (fx+ n pos))
           n)
        (string-set! buf i (string-ref str j)))))
  (define (get-position) pos)
  (define (set-position! new-pos) (set! pos new-pos))
  (define (close)
    (set! str 'closed))
  (assert (string? str))
  (make-custom-textual-input-port "*string*" read! get-position set-position! close))

;;; Binary input

(define (port-fill-buffer p who eof-handling)
  ;; Called iff the port input buffer is empty. Ensures that there is
  ;; at least one item in the buffer and then returns it.
  (assert (input-port? p))
  (cond
    ((and ($port-at-eof p) (eq? eof-handling 'bypass))
     ;; lookahead-* does not consume the eof-object, get-* consumes
     ;; it, get-bytevector-some always tries to read
     (when eof-handling
       ($port-at-eof-set! p #f))
     (eof-object))
    (else
     (let ((b (port-buffer p)))
       (unless (or (string? b) (bytevector? b))
         (assertion-violation who "The input port is closed" p))
       (unless (eqv? 0 (port-buffer-wend p))
         (flush-output-port p)
         (port-buffer-wend-set! p 0))
       ;; FIXME: two fibers might get to here. Block all others until
       ;; the source returns.
       (let* ((source (port-source p))
              (req (if (bytevector? b) (bytevector-length b) (string-length b)))
              (items (source b 0 req)))
         (assert (fx<=? 0 items req))
         (cond ((eqv? items 0)
                ($port-at-eof-set! p #t)
                (eof-object))
               (else
                ($port-at-eof-set! p #f)
                ($port-buffer-rpos-set! p (if (memq who '(get-u8 get-char))
                                              1 0))
                ($port-buffer-rend-set! p items)
                (if (bytevector? b)
                    (bytevector-u8-ref b 0)
                    (string-ref b 0)))))))))

(define get-u8
  (case-lambda
    ((p)
     (unless (binary-input-port? p)
       (assertion-violation 'get-u8 "Expected an open input port" p))
     (let ((r ($port-buffer-rpos p)))
       (cond ((eq? r ($port-buffer-rend p))
              (port-fill-buffer p 'get-u8 #t))
             (else
              ($port-buffer-rpos-set! p (fx+ r 1))
              (bytevector-u8-ref ($port-buffer p) r)))))
    (() (sys:get-u8 (current-input-port*)))))

(define lookahead-u8
  (case-lambda
    ((p)
     (unless (binary-input-port? p)
       (assertion-violation 'lookahead-u8 "Expected an open input port" p))
     (let ((r ($port-buffer-rpos p)))
       (cond ((eq? r ($port-buffer-rend p))
              (port-fill-buffer p 'lookahead-u8 #f))
             (else
              (bytevector-u8-ref ($port-buffer p) r)))))
    (() (sys:lookahead-u8 (current-input-port*)))))

(define ($input-port-ready? p)
  (let ((r ($port-buffer-rpos p)))
    (cond ((eq? r ($port-buffer-rend p))
           ;; R7RS does not have custom ports, so it should be ok if
           ;; this only works on ports with file descriptors. And then
           ;; not even SRFI-181 (custom ports) has char- & u8-ready?.
           ;;
           ;; A complication is that transcoded ports might have extra
           ;; data buffered, so maybe this isn't foolproof.
           (cond
             ((port-file-descriptor p) =>
              (lambda (fd)
                ;; Trickery. The way that wait-for-readable was
                ;; designed isn't really in line with CML principles,
                ;; so it's not actually composable. This uses a trick
                ;; to have cvar-not-readable be signalled only after
                ;; wait-for-readable has had time to check the fd.
                (let ((cvar-readable (make-cvar))
                      (cvar-not-readable (make-cvar)))
                  (spawn-fiber (lambda ()
                                 ;; TODO: This can probably be
                                 ;; lookahead-{char,u8} if the
                                 ;; concurrent access problem is
                                 ;; fixed.
                                 (wait-for-readable fd)
                                 (signal-cvar! cvar-readable)))
                  (spawn-fiber (lambda ()
                                 (let ((next-step (make-cvar)))
                                   (spawn-fiber (lambda ()
                                                  (signal-cvar! next-step)))
                                   (wait next-step)
                                   (signal-cvar! cvar-not-readable))))
                  (perform-operation
                   (choice-operation (wrap-operation
                                      (wait-operation cvar-readable)
                                      (lambda _ #t))
                                     (wrap-operation
                                      (wait-operation cvar-not-readable)
                                      (lambda _ #f)))))))
             (else #t)))
          (else #t))))

(define u8-ready?
  (case-lambda
    ((p)
     (unless (binary-input-port? p)
       (assertion-violation 'u8-ready? "Expected an open binary input port" p))
     ($input-port-ready? p))
    (() (u8-ready? (current-input-port*)))))

(define (bytevector-shrink! src n)
  ;; TODO: do this without copying
  (if (fx=? n (bytevector-length src))
      src
      (let ((dst (make-bytevector n)))
        (bytevector-copy! src 0 dst 0 n)
        dst)))

(define (get-bytevector-n p n)
  (unless (fx>=? n 0)
    (assertion-violation 'get-bytevector-n
                         "Expected a non-negative count" p n))
  (unless (binary-input-port? p)
    (assertion-violation 'get-bytevector
                         "Expected a binary input port" p))
  (let ((buf (port-buffer p))
        (rpos (port-buffer-rpos p))
        (rend (port-buffer-rend p)))
    (cond
      ((eqv? n 0) #vu8())
      ((and (eqv? rpos 0) (fx=? rend n))      ;steal the whole buffer
       (port-buffer-set! p (make-bytevector (bytevector-length buf)))
       (port-buffer-rpos-set! p rend)
       buf)
      (else
       (let ((i (fxmin n (fx- rend rpos)))
             (dst (make-bytevector n)))
         ;; Take as much as possible from the port buffer
         (bytevector-copy! buf rpos dst 0 i)
         (port-buffer-rpos-set! p (fx+ rpos i))
         ;; Use the source directly until eof or the buffer is filled
         (let ((source (port-source p)))
           (let lp ((i i) (remaining (fx- n i)))
             (if (eqv? remaining 0)
                 (if (eqv? i 0) (eof-object) dst)
                 (let ((bytes (source dst i remaining)))
                   (if (eqv? bytes 0)   ;early eof
                       (if (eqv? i 0)
                           (eof-object)
                           (bytevector-shrink! dst i))
                       (lp (fx+ i bytes) (fx- remaining bytes))))))))))))

(define (get-bytevector-n! p dst start n)
  (unless (and (fixnum? n) (not (fxnegative? n))
               (fixnum? n) (not (fxnegative? n)))
    (assertion-violation 'get-bytevector-n!
                         "Expected an exact, non-negative start and count"
                         p dst start n))
  (unless (binary-input-port? p)
    (assertion-violation 'get-bytevector-n!
                         "Expected a binary input port" p))
  (let ((buf (port-buffer p))
        (rpos (port-buffer-rpos p))
        (rend (port-buffer-rend p)))
    (cond
      ((eqv? n 0) 0)
      ((and (eqv? rpos 0) (fx=? rend n))      ;steal the whole buffer
       (port-buffer-set! p (make-bytevector (bytevector-length buf)))
       (port-buffer-rpos-set! p rend)
       buf)
      (else
       (let ((i (fxmin n (fx- rend rpos))))
         ;; Take as much as possible from the port buffer
         (bytevector-copy! buf rpos dst start i)
         (port-buffer-rpos-set! p (fx+ rpos i))
         ;; Use the source directly until eof or the buffer is filled
         (let ((source (port-source p)))
           (let lp ((i (fx+ start i)) (remaining (fx- n i)))
             (if (eqv? remaining 0)
                 (if (eqv? i 0) (eof-object) (fx- i start))
                 (let ((bytes (source dst i remaining)))
                   (if (eqv? bytes 0)   ;early eof
                       (if (eqv? i 0)
                           (eof-object)
                           (fx- i start))
                       (lp (fx+ i bytes) (fx- remaining bytes))))))))))))

(define (get-bytevector-some p)
  (unless (binary-input-port? p)
    (assertion-violation 'get-bytevector-some
                         "Expected a binary input port" p))
  (when (fx=? (port-buffer-rpos p) (port-buffer-rend p))
    (port-fill-buffer p 'get-bytevector-some 'bypass))
  (let ((rpos (port-buffer-rpos p))
        (rend (port-buffer-rend p))
        (buf (port-buffer p)))
    (cond ((fx=? rpos rend)
           (eof-object))
          ((eqv? rpos 0)
           (port-buffer-set! p (make-bytevector (bytevector-length buf)))
           (port-buffer-rpos-set! p rend)
           (bytevector-shrink! buf rend))
          (else
           (let* ((n (fx- rend rpos))
                  (bv (make-bytevector n)))
             (bytevector-copy! buf rpos bv 0 n)
             (port-buffer-rpos-set! p rend)
             bv)))))

(define (get-bytevector-all ip)
  (let ((datum (get-bytevector-n ip 4096)))
    (if (eof-object? datum)
        datum
        (call-with-bytevector-output-port
          (lambda (op)
            (let lp ((datum datum))
              (put-bytevector op datum)
              (let ((datum (get-bytevector-n ip 4096)))
                (unless (eof-object? datum)
                  (lp datum)))))))))

;;; Textual input

(define get-char
  (case-lambda
    ((p)
     (unless (textual-input-port? p)
       (assertion-violation 'get-char "Expected a textual input port" p))
     (let ((rpos ($port-buffer-rpos p)))
       (cond ((fx=? rpos ($port-buffer-rend p))
              (port-fill-buffer p 'get-char #f))
             (else
              ($port-buffer-rpos-set! p (fx+ rpos 1))
              (string-ref ($port-buffer p) rpos)))))
    (() (sys:get-char (current-input-port*)))))

(define lookahead-char
  (case-lambda
    ((p)
     (unless (textual-input-port? p)
       (assertion-violation 'lookahead-char "Expected a textual input port" p))
     (let ((rpos ($port-buffer-rpos p)))
       (cond ((fx=? rpos ($port-buffer-rend p))
              (port-fill-buffer p 'lookahead-char #f))
             (else
              (string-ref ($port-buffer p) rpos)))))
    (() (sys:lookahead-char (current-input-port*)))))

(define char-ready?
  (case-lambda
    ((p)
     (unless (textual-input-port? p)
       (assertion-violation 'char-ready? "Expected an open textual input port" p))
     ($input-port-ready? p))
    (() (char-ready? (current-input-port*)))))

(define (get-string-n port n)
  ;; TODO: optimize the same as get-bytevector-n
  (let ((buf (make-string n)))
    (let lp ((i 0))
      (if (fx=? n i)
          buf
          (let ((c (get-char port)))
            (cond
              ((eof-object? c)
               (if (eqv? i 0)
                   (eof-object)
                   (substring buf 0 i)))
              (else
               (string-set! buf i c)
               (lp (fx+ i 1)))))))))

(define (get-string-n! port buf start n)
  (unless (and (fixnum? start) (not (fxnegative? start))
               (fixnum? n) (not (fxnegative? n)))
    (assertion-violation 'get-string-n!
                         "Expected an exact, non-negative start and count"
                         port buf start n))
  ;; TODO: optimize the same as get-bytevector-n!
  (let ((end (fx+ start n)))
    (unless (fx>=? (string-length buf) end)
      (assertion-violation 'get-string-n!
                           "Expected a string that is at least start+count long"
                           port buf start n))
    (let lp ((i start) (read 0))
      (if (fx=? i end)
          read
          (let ((c (get-char port)))
            (cond ((eof-object? c)
                   (if (eqv? read 0)
                       (eof-object)
                       read))
                  (else
                   (string-set! buf i c)
                   (lp (fx+ i 1) (fx+ read 1)))))))))

(define (get-string-all p)
  ;; TODO: optimize
  (if (port-eof? p)
      (eof-object)
      (call-with-string-output-port
        (lambda (o)
          (let lp ()
            (let ((c (get-char p)))
              (unless (eof-object? c)
                (put-char o c)
                (lp))))))))

(define (get-line p)
  ;; TODO: optimize
  (if (port-eof? p)
      (eof-object)
      (call-with-string-output-port
        (lambda (o)
          (let lp ()
            (let ((c (get-char p)))
              (unless (or (eof-object? c)
                          (eqv? c #\linefeed))
                (put-char o c)
                (lp))))))))

;;; Output ports

(define (flush-output-port p)
  (assert (output-port? p))
  (let ((sink (port-sink p))
        (buffer (port-buffer p)))
    (unless (procedure? sink)
      (error 'flush-output-port "Expected an open output port" p))
    (let lp ((i 0)
             (k (port-buffer-wpos p)))
      (when (fxpositive? k)
        (let ((items (sink buffer i k)))
          (unless (fx<=? 0 items k)
            (error 'put-bytevector "Bad write! return value" p items))
          (let ((i* (fx+ i items))
                (k* (fx- k items)))
            (port-buffer-wpos-set! p i*)
            (lp i* k*))))))
  (port-buffer-rpos-set! p 0)
  (port-buffer-rend-set! p 0)
  (port-buffer-wpos-set! p 0))

(define (output-port-buffer-mode p)
  (assert (output-port? p))
  (port-buffer-mode p))

;; open-file-output-port is defined elsewhere.

(define open-bytevector-output-port
  (case-lambda
    ((transcoder)
     (define buf #vu8())
     (define pos 0)
     (define (grow! minimum)
       (when (fx<? (bytevector-length buf) minimum)
         (let ((new (make-bytevector (max minimum (fx* (bytevector-length buf) 2)))))
           (bytevector-copy! buf 0 new 0 (bytevector-length buf))
           (set! buf new))))
     (define (bytevector-extractor)
       (trace "extract flushes: " buf " " pos)
       (flush-output-port port)
       (trace "extract flushed: " buf " " pos)
       (let ((buffer buf)
             (position pos))
         (set! buf #vu8())
         (set! pos 0)
         (trace "extracting " position " bytes from " buffer)
         (bytevector-shrink! buffer position)))
     (define (write! bv start count)
       (trace "-write! bv=" bv " start=" start " count=" count " buf=" buf)
       (grow! (fx+ pos count))
       (bytevector-copy! bv start buf pos count)
       (set! pos (+ pos count))
       (trace "+write! bv=" bv " start=" start " count=" count " buf=" buf)
       count)
     (define (get-position)
       pos)
     (define (set-position! pos)
       (error 'set-position! "TODO: bytevector output ports" pos))
     (define (close)
       (set! buf #vu8())
       (set! pos 0))
     (define port
       ;; TODO: instead of *bytevector* name it after the caller
       (let ((p (make-custom-binary-output-port "*bytevector*"
                                                write! get-position
                                                set-position! close)))
         (if transcoder
             (transcoded-port p transcoder)
             p)))
     (values port bytevector-extractor))
    (()
     (open-bytevector-output-port #f))))

(define call-with-bytevector-output-port
  (case-lambda
    ((proc)
     (call-with-bytevector-output-port proc #f))
    ((proc transcoder)
     (let-values (((port extractor) (open-bytevector-output-port transcoder)))
       (proc port)
       (flush-output-port port)
       (let ((ret (extractor)))
         (close-port port)
         ret)))))

(define (open-string-output-port)
  (define buf "")
  (define pos 0)
  (define (grow! minimum)
    (when (fx<? (string-length buf) minimum)
      (do ((new (make-string (max minimum (fx* (string-length buf) 2))))
           (i 0 (fx+ i 1)))
          ((fx=? i (string-length buf))
           (set! buf new))
        (string-set! new i (string-ref buf i)))))
  (define (string-extractor)
    ;; TODO: the buffer could actually be shrunk by the GC
    (flush-output-port port)
    (let ((buffer buf)
          (position pos))
      (set! buf "")
      (set! pos 0)
      (trace "extracting " position " chars from '" buffer "'")
      (if (fx=? (string-length buffer) position)
          buffer
          (do ((ret (make-string position))
               (i 0 (fx+ i 1)))
              ((fx=? i position)
               ret)
            (string-set! ret i (string-ref buffer i))))))
  (define (write! str start count)
    (trace "-write! string=" str " start=" start " count=" count " buf='" buf "'")
    (grow! (fx+ pos count))
    (do ((i 0 (fx+ i 1)))
        ((fx=? i count))
      (string-set! buf (fx+ pos i) (string-ref str (fx+ start i))))
    (set! pos (+ pos count))
    (trace "+write! string=" str " start=" start " count=" count " buf='" buf "'")
    count)
  (define (close)
    (set! buf "")
    (set! pos 0))
  (define port
    (make-custom-textual-output-port "*string*" write! #f #f close))
  (port-extractor-set! port string-extractor)
  ;; TODO: port-position
  (values port string-extractor))

(define (call-with-string-output-port proc)
  (let-values (((port extractor) (open-string-output-port)))
    (proc port)
    (let ((ret (extractor)))
      (close-port port)
      ret)))

;;; SRFI 6-style string ports

(define (open-output-string)
  (let-values ([(port _) (open-string-output-port)])
    port))

(define (get-output-string port)
  (let ((extractor (port-extractor port)))
    (unless extractor
      (assertion-violation 'get-output-string
                           "Expected a string output port" port))
    (extractor)))

;;; Binary output

(define (port-output-overflow-u8 p u8 who)
    (unless (output-port-open? p)
      (assertion-violation who "Expected an open port" p u8))
    (flush-output-port p)
    (bytevector-u8-set! (port-buffer p) 0 u8)
    (port-buffer-wpos-set! p 1)
    (case (port-buffer-mode p)
      ((none)
       (flush-output-port p)))
    (unless (eq? (port-buffer-mode p) 'none)
      (let ((buf (port-buffer p)))
        (port-buffer-wend-set! p (if (string? buf)
                                     (string-length buf)
                                     (bytevector-length buf))))))

(define (put-u8 p u8)
  (unless (binary-output-port? p)
    (assertion-violation 'put-u8 "Expected a binary output port" p u8))
  (let ((wpos (port-buffer-wpos p)))
    (cond ((fx=? wpos (port-buffer-wend p))
           (port-output-overflow-u8 p u8 'put-u8))
          (else
           (bytevector-u8-set! (port-buffer p) wpos u8)
           (port-buffer-wpos-set! p (fx+ wpos 1))))))

(define put-bytevector
  (case-lambda
    ((p bv)
     (put-bytevector p bv 0 (bytevector-length bv)))
    ((p bv start)
     (put-bytevector p bv start (fx- (bytevector-length bv) start)))
    ((p bv start count)
     (unless (binary-output-port? p)
       (error 'put-bytevector "Expected a binary output port" p bv start count))
     (let ((end (fx+ start count))
           (buf (port-buffer p)))
       (assert (fx<=? 0 start end (bytevector-length bv)))
       (let ((wend (port-buffer-wend p))
             (wpos (port-buffer-wpos p)))
         (when (fx=? wpos wend)
           (flush-output-port p))
         (trace "put-bytevector " p " bv=" bv " start=" start " count=" count
                " end=" end " mode=" mode)
         (cond
           ((fx<? count wend)
            ;; The data will fit in the buffer, possibly after
            ;; flushing. Reduces calls to the sink.
            (unless (fx<? count (fx- wend wpos))
              (flush-output-port p))
            (let ((wpos (port-buffer-wpos p)))
              (bytevector-copy! bv start buf wpos count)
              (port-buffer-wpos-set! p (fx+ wpos count))))
           (else
            ;; The data to write is larger than the buffer, so flush
            ;; the buffer and use the sink directly. This also reduces
            ;; calls to the sink, assuming the sink can handle
            ;; relatively large buffers.
            (flush-output-port p)
            (let ((sink (port-sink p)))
              (let lp ((i start)
                       (k count))
                (when (fxpositive? k)
                  (let ((bytes (sink bv i k)))
                    (unless (fx<=? 0 bytes k)
                      (error 'put-bytevector "Bad write! return value" p bytes))
                    (let ((i* (fx+ i bytes))
                          (k* (fx- k bytes)))
                      (lp i* k*)))))))))))))

;;; Textual output

(define (port-output-overflow-char p c who)
  (unless (output-port-open? p)
    (assertion-violation who "Expected an open port" p c))
  (when (and (not (eqv? 0 (port-buffer-wpos p)))
             (fx=? (port-buffer-wpos p) (port-buffer-wend p)))
    (flush-output-port p))
  (let ((wpos (port-buffer-wpos p)))
    (string-set! (port-buffer p) wpos c)
    (port-buffer-wpos-set! p (fx+ wpos 1)))
  (when (or (eq? c (port-nl p))
            (fx>=? (port-buffer-wpos p) (port-buffer-wend p)))
    (flush-output-port p)))

(define (put-char p c)
  (unless (char? c)
    (assertion-violation 'put-char "Expected a character" p c))
  (unless (textual-output-port? p)
    (assertion-violation 'put-char "Expected a textual output port" p c))
  (let ((wpos (port-buffer-wpos p)))
    (cond ((or (fx=? wpos (port-buffer-wend p))
               (eq? c (port-nl p)))
           (port-output-overflow-char p c 'put-char))
          (else
           (string-set! (port-buffer p) wpos c)
           (port-buffer-wpos-set! p (fx+ wpos 1))))))

(define put-string
  (case-lambda
    ((p bv)
     (put-string p bv 0 (string-length bv)))
    ((p bv start)
     (put-string p bv start (fx- (string-length bv) start)))
    ((p bv start count)
     (let ((end (fx+ start count)))
       (assert (fx<=? 0 start end (string-length bv)))
       (do ((i start (fx+ i 1)))
           ((fx=? i end))
         (put-char p (string-ref bv i)))))))

(define (put-datum tp d)
  (write d tp))

;;; Input/output ports

;; open-file-input/output-port is defined elsewhere.

;;; Simple I/O

(define (call-with-input-file filename proc)
  (let ((p (open-input-file filename)))
    (let-values ((ret (proc p)))
      (close-input-port p)
      (apply values ret))))

(define (call-with-output-file filename proc)
  (let ((p (open-output-file filename)))
    (let-values ((ret (proc p)))
      (close-output-port p)
      (apply values ret))))

(define (with-input-from-file filename thunk)
  (call-with-port (open-input-file filename)
    (lambda (p)
      (parameterize ([current-input-port* p])
        (thunk)))))

(define (with-output-to-file filename thunk)
  (call-with-port (open-output-file filename)
    (lambda (p)
      (parameterize ([current-output-port* p])
        (thunk)))))

(define (open-input-file filename)
  (open-file-input-port filename
                        (file-options)
                        (buffer-mode block)
                        (native-transcoder)))

(define (open-output-file filename)
  (open-file-output-port filename
                         (file-options)
                         (buffer-mode block)
                         (native-transcoder)))

(define (close-input-port p)
  (assert (input-port? p))
  (close-port p))

(define (close-output-port p)
  (assert (output-port? p))
  (close-port p))

(define read-char
  (case-lambda
    (() (get-char (current-input-port*)))
    ((tp) (get-char tp))))

(define peek-char
  (case-lambda
    (() (lookahead-char (current-input-port*)))
    ((tp) (lookahead-char tp))))

(define write-char
  (case-lambda
    ((ch) (put-char (current-output-port*) ch))
    ((ch tp) (put-char tp ch))))

(define newline
  (case-lambda
    (() (write-char #\linefeed))
    ((p) (write-char #\linefeed p))))

(define-syntax %bytevector
  (lambda (x)
    (syntax-case x ()
      ((_ v)
       (string? (syntax->datum #'v))
       (string->utf8 (syntax->datum #'v))))))

(define (display-hex24 i p)
  ;; Displays 24-bit integers in hexadecimal notation
  (define chars (%bytevector "0123456789ABCDEF"))
  (when (eqv? i 0) (put-char p #\0))
  (let lp ((s 24) (lz #t))
    (unless (fx<? s 0)
      (let* ((nibble (fxand (fxarithmetic-shift-right i s) #xf))
             (lz (and lz (eqv? nibble 0))))
        (when (not lz)
          (put-char p (integer->char (bytevector-u8-ref chars nibble))))
        (lp (fx- s 4) lz)))))

(define (display-char-escape c p)
  (put-char p #\\)
  (put-char p #\x)
  (display-hex24 (char->integer c) p)
  (put-char p #\;))

(define (write-symbol name p)
  ;; FIXME: handle latin-1 ports
  (define (char-initial? c)
    (or (char<=? #\a c #\z)
        (char<=? #\A c #\Z)
        (memv c '(#\! #\$ #\% #\& #\* #\/ #\: #\< #\= #\> #\? #\^ #\_ #\~))
        (and (fx>? (char->integer c) 127)
             (memq (char-general-category c)
                   '(Lu Ll Lt Lm Lo Mn Nl No Pd Pc Po Sc Sm Sk So Co)))))
  (define (char-subsequent? c)
    (or (char<=? #\a c #\z)
        (char<=? #\A c #\Z)
        (char<=? #\0 c #\9)
        (memv c '(#\! #\$ #\% #\& #\* #\/ #\: #\< #\= #\> #\? #\^ #\_ #\~
                  #\+ #\- #\. #\@))
        (and (fx>? (char->integer c) 127)
             (memq (char-general-category c)
                   '(Lu Ll Lt Lm Lo Mn Nl No Pd Pc Po Sc Sm Sk So Co
                        Nd Mc Me)))))
  (cond ((eqv? 0 (string-length name))
         (display "||" p))                  ;not in R6RS
        ((member name '("+" "-" "..."))
         (display name p))
        (else
         (let ((c0 (string-ref name 0)))
           (if (or (char-initial? c0)
                   (and (eqv? c0 #\-)
                        (fx>=? (string-length name) 2)
                        (eqv? (string-ref name 1) #\>)))
               (put-char p c0)
               (display-char-escape c0 p)))
         (do ((i 1 (fx+ i 1)))
             ((fx=? i (string-length name)))
           (let ((c (string-ref name i)))
             (if (not (char-subsequent? c))
                 (display-char-escape c p)
                 (put-char p c)))))))

(define (display* v p write?)
  (define (char-unprintable? c)
    ;; TODO: unicode
    ;; some are unprintable as chars, some in strings too.
    ;; which ones are printable depends on the encoding
    ;; of the output port!
    (let ((v (char->integer c)))
      (or (fx<? v (char->integer #\space))
          (fx<? #x7e v #xA0))))
  (define chars
    '#("nul" #f #f #f #f #f #f "alarm" "backspace" "tab"
       "linefeed" "vtab" "page" "return" #f #f #f #f #f #f #f #f
       #f #f #f #f #f "esc" #f #f #f #f "space" #f #f #f #f #f #f
       #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f
       #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f
       #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f
       #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f
       #f #f #f #f #f #f #f #f #f #f #f #f "delete"))
  (define string-escapes
    (%bytevector
     "XXXXXXXabtnvfrXXXXXXXXXXXXXXXXXXXX\"XXXXXXXXXXXXXXXXXXXXX\
        XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\\"))
  (cond
    ((char? v)
     (cond (write?
            (put-char p #\#)
            (put-char p #\\)
            (let ((i (char->integer v)))
              (cond ((and (fx<? i (vector-length chars))
                          (vector-ref chars i))
                     => (lambda (str) (put-string p str)))
                    ((char-unprintable? v)
                     (put-char p #\x)
                     (display-hex24 i p))
                    (else
                     (put-char p v)))))
           (else
            (put-char p v))))
    ((bytevector? v)
     (if (memq 'r7rs (print-dialects))
         (display "#u8(" p)
         (display "#vu8(" p))
     (let ((len (bytevector-length v)))
       (unless (eqv? len 0)
         (display (bytevector-u8-ref v 0) p)
         (do ((i 1 (fx+ i 1)))
             ((fx=? i len))
           (put-char p #\space)
           (display (bytevector-u8-ref v i) p))))
     (put-char p #\)))
    ((null? v)
     (put-char p #\()
     (put-char p #\)))
    ((boolean? v)
     (put-char p #\#)
     (if v (put-char p #\t) (put-char p #\f)))
    ((eof-object? v)
     (display "#!eof" p))
    (($void? v)
     (display "#<void #x" p)
     (display (number->string ($void->fixnum v) 16) p)
     (display ">" p))
    (($immsym? v)
     (let ((alphabet     "abcdefghijklmnopqrstuvwxyz-/<=>")
           (end-alphabet "acdefghklmnopqrstvxy!*+-/08<=>?"))
       (let ((s ($immsym->fixnum v)))
         (let ((s (fxarithmetic-shift-right s 5))
               (c (fx- (fxand s #b11111) 1)))
           ;; The first character might need to be escaped
           (let ((ch (string-ref (if (eqv? s 0) end-alphabet alphabet) c)))
             (if (and write? (or (memv ch '(#\0 #\8))
                                 (and (not (eqv? s 0)) (memv ch '(#\- #\+)))))
                 (display-char-escape ch p)
                 (put-char p ch)))
           ;; Output the rest of the characters, if there are any
           (let lp ((s s))
             (unless (eqv? s 0)
               (let ((s (fxarithmetic-shift-right s 5))
                     (c (fx- (fxand s #b11111) 1)))
                 (cond ((eqv? s 0)
                        (put-char p (string-ref end-alphabet c)))
                       (else
                        (put-char p (string-ref alphabet c))
                        (lp s))))))))))
    ((string? v)
     (if write? (put-char p #\"))
     (do ((len (string-length v))
          (i 0 (fx+ i 1)))
         ((fx=? i len))
       (let* ((c (string-ref v i))
              (v (char->integer c)))
         (cond ((and write? (fx<? v (bytevector-length string-escapes))
                     (not (eqv? (bytevector-u8-ref string-escapes v)
                                (char->integer #\X))))
                (put-char p #\\)
                (put-char p (integer->char (bytevector-u8-ref string-escapes v))))
               ((and write? (char-unprintable? c))
                (display-char-escape c p))
               (else
                (put-char p c)))))
     (if write? (put-char p #\")))
    ((pair? v)
     (cond ((and (pair? (cdr v))
                 (null? (cddr v))
                 (assq (car v) '((quote . "'")
                                 (quasiquote . "`")
                                 (unquote . ",")
                                 (unquote-splicing . ",@")
                                 (syntax . "#'")
                                 (quasisyntax . "#`")
                                 (unsyntax . "#,")
                                 (unsyntax-splicing . "#,@"))))
            => (lambda (a)
                 (display (cdr a) p)
                 (display* (cadr v) p write?)))
           (else
            (put-char p #\()
            (let lp ((i v))
              (unless (null? i)
                (display* (car i) p write?)
                (let ((i (cdr i)))
                  (cond ((null? i))
                        ((pair? i)
                         (put-char p #\space)
                         (lp i))
                        (else
                         (put-char p #\space)
                         (put-char p #\.)
                         (put-char p #\space)
                         (display* i p write?))))))
            (put-char p #\)))))
    ((vector? v)
     (put-char p #\#) (put-char p #\()
     (let ((len (vector-length v)))
       (unless (eqv? len 0)
         (display* (vector-ref v 0) p write?)
         (do ((i 1 (fx+ i 1)))
             ((fx=? i len))
           (put-char p #\space)
           (display* (vector-ref v i) p write?))))
     (put-char p #\)))
    ((number? v)
     ($display-number v p))
    ((procedure? v)
     (let ((info ($procedure-info v)))
       (define (info? x) (and ($box? x) (eq? ($box-type x) 'info)))
       (define (info-free-length x) ($box-ref x 1))
       (define (info-name x) ($box-ref x 2))
       (define (info-source x) ($box-ref x 3))
       (cond ((info? info)
              (if (eqv? (info-free-length info) 0)
                  (display "#<procedure " p)
                  (display "#<closure " p))
              (if (info-name info)
                  (display (info-name info) p)
                  (display "(anonymous)" p))
              (when (info-source info)
                (put-char p #\space)
                (display (vector-ref (info-source info) 0) p)
                (put-char p #\:)
                (display (vector-ref (info-source info) 1) p)
                (put-char p #\:)
                (display (vector-ref (info-source info) 2) p)))
             (else
              ;; really shouldn't happen
              (display "#<procedure>" p))))
     (display ">" p))
    ((port? v)
     ;; [binary|textual]-[input|output|input/output]-port
     (display "#<" p)
     (if (textual-port? v)
         (display "textual-" p)
         (display "binary-" p))
     (if (input-port? v)
         (if (output-port? v)
             (display "input/output-port " p)
             (display "input-port " p))
         (display "output-port " p))
     (write (port-id v) p)
     (display " buffer-mode: " p)
     (display (port-buffer-mode v) p)
     (cond ((port-file-descriptor v) =>
            (lambda (fd)
              (display " fd: " p)
              (display fd p))))
     (when (not (port-buffer v))
       (display " (closed)" p))
     (display #\> p))
    (($box? v)
     (let ((t ($box-type v)))
       (cond ((symbol? t)
              ;; FIXME: This case should be phased out
              (display "#<" p)
              (display ($box-type v) p)
              (when (eq? ($box-type v) 'rtd)
                (display " " p)
                (write ($box-ref v 2) p))
              (display ">" p))
             (($box-header-type-eq? t 'symbol)
              ;; Symbol
              (cond ((and write? (gensym? v))
                     ;; Gensym write syntax
                     (let ((symbol-name (gensym-prefix v))
                           (unique-string (gensym->unique-string v)))
                       (display "#{" p)
                       (display symbol-name p)
                       (display " |" p)
                       (display unique-string p)
                       (display "|}" p)))
                    (else
                     (let ((name (symbol->string v)))
                       (if write?
                           (write-symbol name p)
                           (display name p))))))
             ((record-type-descriptor? t)
              (let ((writer (record-writer t)))
                (writer v p write)))
             (else
              (display "#<box " p)
              (display t p)
              (display ">" p)))))
    (else
     (display "#<unknown>" p))))

(define display
  (case-lambda
    ((v) (display* v (current-output-port*) #f))
    ((v p) (display* v p #f))))

(define write
  (case-lambda
    ((v)
     (write v (current-output-port*)))
    ((v p)
     (let ((counter -1)
           (shared (find-cycles v)))
       (if (eqv? 0 (hashtable-size shared))
           (display* v p #t)
           (letrec ((increment!
                     (lambda ()
                       (set! counter (fx+ counter 1))
                       counter)))
             ;; Cycles were detected.
             (write/c v p shared increment!)))))))

(define write-shared
  (case-lambda
    ((v)
     (write-shared v (current-output-port*)))
    ((v p)
     (let ((counter -1)
           (shared (find-shared v)))
       (if (eqv? 0 (hashtable-size shared))
           (display* v p #t)
           (letrec ((increment!
                     (lambda ()
                       (set! counter (fx+ counter 1))
                       counter)))
             ;; Shared structures were detected.
             (write/c v p shared increment!)))))))

(define write-simple
  (case-lambda
    ((v)
     (write-simple v (current-output-port*)))
    ((v p)
     (display* v p #t))))

;; Print with cycles in pairs, vectors and records.
(define (write/c v p shared increment!)
  (define (write-prefix id)
    (put-char p #\#)
    (display id p)
    (put-char p #\=))
  (define (write-ref id)
    (put-char p #\#)
    (display id p)
    (put-char p #\#))
  (define (number! v)
    (let ((id (hashtable-ref shared v #f)))
      (when (eq? id 'must-assign)
        (let ((id (increment!)))
          (write-prefix id)
          (hashtable-set! shared v id)))
      id))
  (let ((id (number! v)))
    (cond
      ((fixnum? id)
       (write-ref id))
      ((pair? v)
       (cond ((and (pair? (cdr v))
                   (null? (cddr v))
                   (assq (car v) '((quote . "'")
                                   (quasiquote . "`")
                                   (unquote . ",")
                                   (unquote-splicing . ",@")
                                   (syntax . "#'")
                                   (quasisyntax . "#`")
                                   (unsyntax . "#,")
                                   (unsyntax-splicing . "#,@"))))
              => (lambda (a)
                   (display (cdr a) p)
                   (write/c (cadr v) p shared increment!)))
             (else
              ;; FIXME: cycles are not detected here
              (put-char p #\()
              (let lp ((i v))
                (unless (null? i)
                  (write/c (car i) p shared increment!)
                  (let ((i (cdr i)))
                    (cond ((null? i))
                          ((pair? i)
                           (let ((id (number! i)))
                             (cond ((fixnum? id)
                                    (put-char p #\space)
                                    (put-char p #\.)
                                    (put-char p #\space)
                                    (write-ref id))
                                   (else
                                    (put-char p #\space)
                                    (lp i)))))
                          (else
                           (put-char p #\space)
                           (put-char p #\.)
                           (put-char p #\space)
                           (write/c i p shared increment!))))))
              (put-char p #\)))))
      ((vector? v)
       (put-char p #\#) (put-char p #\()
       (let ((len (vector-length v)))
         (unless (eqv? len 0)
           (write/c (vector-ref v 0) p shared increment!)
           (do ((i 1 (fx+ i 1)))
               ((fx=? i len))
             (put-char p #\space)
             (write/c (vector-ref v i) p shared increment!))))
       (put-char p #\)))
      ((record? v)
       (let* ((t ($box-type v))
              (writer (record-writer t)))
         (writer v p (lambda (v p) (write/c v p shared increment!)))))
      (else
       (write v p)))))

(define (open-textual-null-port)
  (define (write! _buf _start count) count)
  (define (read! _buf _start _count) 0)
  (make-custom-textual-input/output-port "*null*" read! write! #f #f #f))

;; Build a hashtable that maps objects to 'must-assign if they appear
;; inside themselves.
(define (find-cycles v)
  (define dummy (open-textual-null-port))
  (let ((s (make-eq-hashtable)))
    (let f ((v v))
      (when (or (pair? v) (vector? v) (record? v))
        (cond ((hashtable-ref s v #f) =>
               (lambda (id)
                 (when (eq? id 'unassigned)
                   (hashtable-set! s v 'must-assign))))
              ((pair? v)
               (hashtable-set! s v 'unassigned)
               (f (car v))
               (f (cdr v)))
              ((vector? v)
               (when (fxpositive? (vector-length v))
                 (hashtable-set! s v 'unassigned)
                 (vector-for-each f v)))
              ((record? v)
               (hashtable-set! s v 'unassigned)
               (let* ((t ($box-type v))
                      (writer (record-writer t)))
                 (writer v dummy (lambda (v _) (f v))))))
        (when (eq? (hashtable-ref s v #f) 'unassigned)
          ;; The object does not contain a reference to itself.
          (hashtable-set! s v #f))))
    s))

;; Build a hashtable that maps (some) objects to 'must-assign if they
;; appear more than once in the structure.
(define (find-shared v)
  (define dummy (open-textual-null-port))
  (let ((s (make-eq-hashtable)))
    (let f ((v v))
      (when (or (pair? v)
                (and (vector? v) (not (eqv? 0 (vector-length v))))
                (record? v)
                (and (string? v) (not (eqv? 0 (string-length v)))))
        (cond ((hashtable-ref s v #f) =>
               (lambda (id)
                 (when (eq? id 'unassigned)
                   (hashtable-set! s v 'must-assign))))
              ((pair? v)
               (hashtable-set! s v 'unassigned)
               (f (car v))
               (f (cdr v)))
              ((vector? v)
               (when (fxpositive? (vector-length v))
                 (hashtable-set! s v 'unassigned)
                 (vector-for-each f v)))
              ((record? v)
               (hashtable-set! s v 'unassigned)
               (let* ((t ($box-type v))
                      (writer (record-writer t)))
                 (writer v dummy (lambda (v _) (f v)))))
              ((string? v)
               (hashtable-set! s v 'unassigned)))))
    s))

;;; File system

;; See (loko runtime init).

;;; Standard ports

;; These are registered with $init-standard-ports. Each new process
;; will get its own set of these.
(define *stdin-read*)
(define *stdout-write*)
(define *stderr-write*)
(define *stdout-bufmode* (buffer-mode line))

(define (standard-input-port)
  (let ((p (make-custom-binary-input-port "*stdin*" *stdin-read* #f #f #f)))
    (port-file-descriptor-set! p 0)
    p))

(define (standard-output-port)
  (let ((p (make-custom-binary-output-port "*stdout*" *stdout-write* #f #f #f)))
    (port-file-descriptor-set! p 1)
    ($port-buffer-mode-set! p *stdout-bufmode*)
    p))

(define (standard-error-port)
  (let ((p (make-custom-binary-output-port "*stderr*" *stderr-write* #f #f #f)))
    (port-file-descriptor-set! p 2)
    ($port-buffer-mode-set! p (buffer-mode line))
    p))

(define current-output-port*
  (make-parameter (open-textual-null-port)
                  (lambda (p)
                    (assert (textual-output-port? p))
                    p)))

(define current-error-port*
  (make-parameter (open-textual-null-port)
                  (lambda (p)
                    (assert (textual-output-port? p))
                    p)))

(define current-input-port*
  (make-parameter (open-textual-null-port)
                  (lambda (p)
                    (assert (textual-input-port? p))
                    p)))

(define (current-output-port) (current-output-port*))
(define (current-error-port) (current-error-port*))
(define (current-input-port) (current-input-port*))

(define ($init-standard-ports stdin-read stdout-write stderr-write
                              stdout-bufmode eolstyle)
  (set! *stdin-read* stdin-read)
  (set! *stdout-write* stdout-write)
  (set! *stderr-write* stderr-write)
  (set! *stdout-bufmode* stdout-bufmode)
  (let ((tc (make-transcoder (utf-8-codec) eolstyle (error-handling-mode replace))))
    (current-input-port* (transcoded-port (standard-input-port) tc))
    (current-output-port* (transcoded-port (standard-output-port) tc))
    (current-error-port* (transcoded-port (standard-error-port) tc))))

($init-standard-ports (lambda _ 0)
                      (lambda (bv start count)
                        (do ((end (+ start count))
                             (i start (fx+ i 1)))
                            ((fx=? i end) count)
                          ($debug-put-u8 (bytevector-u8-ref bv i))))
                      (lambda (_bv _start count) count)
                      (buffer-mode none)
                      (native-eol-style)))
