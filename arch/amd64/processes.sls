;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: AGPL-3.0-or-later
;; Loko Scheme - an R6RS Scheme compiler
;; Copyright © 2019, 2020 Göran Weinholt

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
#!r6rs

;;; Process control

(library (loko arch amd64 processes)
  (export
    ;; process stuff
    $process-start)
  (import
    (rnrs (6))
    (only (loko) machine-type)
    (loko arch amd64 memory)
    (loko system unsafe)
    (loko system $primitives)
    (except (loko system $host) $process-start))

;; This should be split: the mapping and the starting. After mapping
;; and setting up the stack frame and pointer, just put it in the
;; process queue. The fixed mappings might not be such a good idea.

(define ($process-start area)
  ;; TODO: most of this should be replaced with a real virtual
  ;; memory address allocator.
  (define K 1024)
  (define M (* 1024 K))
  (define G (* 1024 M))
  (define T (* 1024 G))
  (define PAGE-SIZE (* 4 K))
  (define CODE-TOP (* 4 G))
  (define MARK-AND-SWEEP-TOP (* 1 T))
  (define STACK-SIZE (* 128 M))
  ;; XXX: This is a short-sighted fix to allow self-compilation
  (define HEAP-SIZE (if (not (eq? (vector-ref (machine-type) 1) 'pc))
                        (* 1024 M)
                        (* 256 M)))
  (define VIRTUAL-ADDRESS-TOP (expt 2 (- 48 1)))
  (define (stack-area n)
    (assert (and (fixnum? n) (not (fxnegative? n))))
    (let ((start (fx+ MARK-AND-SWEEP-TOP (fx* n (fx+ HEAP-SIZE STACK-SIZE)))))
      (when (fx>? (fx+ start (fx+ HEAP-SIZE STACK-SIZE)) VIRTUAL-ADDRESS-TOP)
        (error 'stack-area "There is no stack area with this number" n))
      start))
  (define (heap-area n)
    (assert (and (fixnum? n) (not (fxnegative? n))))
    (let ((start (fx+ (fx+ MARK-AND-SWEEP-TOP STACK-SIZE) (fx* n (fx+ HEAP-SIZE STACK-SIZE)))))
      (when (fx>? (fx+ start (fx+ HEAP-SIZE STACK-SIZE)) VIRTUAL-ADDRESS-TOP)
        (error 'heap-area "There is no heap area with this number" n))
      start))
  (define (print . x) (for-each display x) (newline))
  (define PROCESS-SAVE-SIZE 4096)
  (let ((start-current-heap (heap-area area))
        (size-current-heap (fxdiv HEAP-SIZE 2))
        (start-other-heap (fx+ (heap-area area) (fxdiv HEAP-SIZE 2)))
        (size-other-heap (fxdiv HEAP-SIZE 2))
        (stack-bottom (fx+ (stack-area area) 4096))
        (stack-top (fx+ (stack-area area) (fx- STACK-SIZE PROCESS-SAVE-SIZE)))
        (stack-size (fx- (fx- STACK-SIZE PROCESS-SAVE-SIZE) 4096)))
    ;; (print "stack-bottom: " (number->string stack-bottom 16))
    ($mmap stack-bottom (+ stack-size PROCESS-SAVE-SIZE) 'stack)
    ($mmap (heap-area area) HEAP-SIZE 'heap)
    (let ((rflags (bitwise-ior (expt 2 18)  ;Alignment check
                               (expt 2 9))) ;Interrupt flag
          (r14 start-current-heap)
          (r13 size-current-heap)
          (start ($linker-address 'scheme-start))
          (rip ($linker-address 'process-init)))
      (let* ((sp (fx+ (stack-area area) (fx- STACK-SIZE PROCESS-SAVE-SIZE)))
             ;; The routine called to start up the Scheme
             (sp (fx- sp 8)) (_ (put-mem-s61 sp start))
             ;; Must have RFLAGS to enable #AC and interrupts
             ;; (relevant if running directly on the hardware).
             (sp (fx- sp 8)) (_ (put-mem-s61 sp rflags))
             ;; Memory management stuff for process vector
             (sp (fx- sp 8)) (_ (put-mem-s61 sp stack-size))
             (sp (fx- sp 8)) (_ (put-mem-s61 sp stack-top))
             (sp (fx- sp 8)) (_ (put-mem-s61 sp size-other-heap))
             (sp (fx- sp 8)) (_ (put-mem-s61 sp start-other-heap))
             (sp (fx- sp 8)) (_ (put-mem-s61 sp size-current-heap))
             (sp (fx- sp 8)) (_ (put-mem-s61 sp start-current-heap))
             ;; Memory management registers
             (sp (fx- sp 8)) (_ (put-mem-s61 sp r13))
             (sp (fx- sp 8)) (_ (put-mem-s61 sp r14))
             ;; Process entry point
             (sp (fx- sp 8)) (_ (put-mem-s61 sp rip)))
        ;; XXX: could send boot-data the same way Unix does it
        sp)))))
