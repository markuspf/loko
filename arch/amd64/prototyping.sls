;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: AGPL-3.0-or-later
;; Loko Scheme - an R6RS Scheme compiler
;; Copyright © 2019, 2020 Göran Weinholt

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
#!r6rs

;;; Stuff for prototyping on the amd64 port (in the REPL).

;; This is maybe temporary until the compiler is up and running.

(library (loko arch amd64 prototyping)
  (export
    cpuid rdtsc rdrand rdseed

    $processor-data-ref $processor-data-set!
    $object->fixnum

    $box?
    $make-box
    $make-box-header
    $box-ref
    $box-set!
    $box-type
    $box-type-set!
    $box-header-type-eq?
    $box-header-value)
  (import
    (rnrs)
    (prefix (loko system $x86) sys:)
    (prefix (loko system $primitives) sys:)
    (prefix (loko system $host) sys:))

(define cpuid
  (case-lambda
    ((eax)
     (cpuid eax 0))
    ((eax ecx)
     (sys:cpuid eax ecx))))

(define (rdtsc)
  (sys:rdtsc))

(define (rdrand)
  (sys:rdrand))

(define (rdseed)
  (sys:rdseed))

(define ($processor-data-ref idx)
  (sys:$processor-data-ref idx))

(define ($processor-data-set! idx v)
  (sys:$processor-data-set! idx v))

(define ($object->fixnum x)
  (sys:$object->fixnum x))

;; The boxes!! Should probably not be here either.
(define ($box? x) (sys:$box? x))

(define ($make-box type len) (sys:$make-box type len))

(define ($make-box-header type refs? value length)
  (case type
    ((bignum) (sys:$make-box-header 'bignum refs? value length))
    ((ratnum) (sys:$make-box-header 'ratnum refs? value length))
    ((symbol) (sys:$make-box-header 'symbol refs? value length))
    ((port) (sys:$make-box-header 'port refs? value length))
    (else
     (error '$make-box-header "Not implemented" type refs? value length))))

(define ($box-ref v i) (sys:$box-ref v i))

(define ($box-set! v i x) (sys:$box-set! v i x))

(define ($box-type x) (sys:$box-type x))

(define ($box-type-set! x t) (sys:$box-type-set! x t))

(define ($box-header-value x) (sys:$box-header-value x))

(define $box-header-type-eq?
  (case-lambda
    ((obj type)
     (case type
       ((port) (sys:$box-header-type-eq? obj 'port))
       (else
        (error '$box-header-type-eq? "Not implemented" obj type))))
    ((obj type mask test)
     (case type
       ((port)
        (and (sys:$box-header-type-eq? obj 'port)
             (let ((t ($box-header-value obj)))
               (fx=? test (fxand t mask)))))
       (else
        (error '$box-header-type-eq? "Not implemented" obj type mask test)))))))
