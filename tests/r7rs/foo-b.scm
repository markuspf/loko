;; Copyright © 2020 Göran Weinholt
;; SPDX-License-Identifier: AGPL-3.0-or-later
(define FOO-B
  (begin
    (set! counter (+ counter 1))
    counter))
